<div id="app">
  <input id="nonce" type="hidden" name="" value="<?php echo $nonce ?>">
  <div  class="kanguplugin_reservation_popup">
    <div class="kanguplugin_reservation_modal">
      <div class="kanguplugin_head">
        <?php _e('Rezerwacja','kanguplugin') ?> <span id="close"><i class="fa fa-times-circle-o" aria-hidden="true"></i></span>

      </div>

      <div class="col-md-12 groups">
        <p><strong><?php _e('Wybierz grupę:','kanguplugin') ?></strong> </p>
        <a id="changeGroup" href="#"><?php _e('Zmień grupę','kanguplugin') ?></a>
        <?php foreach ($groups as $group): ?>
          <?php $options = $group['options'] ?>
          <div data-<?php echo $options[0]['id'] ?>="<?php echo $options[0]['val'] ?>" data-<?php echo $options[1]['id'] ?>="<?php echo $options[1]['val'] ?>" data-id="<?php echo $group['id'] ?>" class="usersGroup">
            <strong><?php echo $group['name'] ?></strong>

          </div>
        <?php endforeach; ?>
      </div>

      <div id="datesDiv" class="col-md-12 dates kanguplugin_hide">
        <p><strong><?php _e('Wybierz datę','kanguplugin') ?></strong> </p>
        <input id="date" type="text" name="" value="<?php echo $today ?>">
        <button id="check" type="button" name="button"><?php _e('Kontynuuj','kanguplugin') ?></button>
      </div>

      <div id="hoursDiv" class="col-md-12 kanguplugin_hide">
        <img class="loader" src="/wp-admin/images/loading.gif" alt="">
        <div id="hours">

        </div>
      </div>

      <div id="quantities" class=" col-md-12 kanguplugin_hide">
        <p><?php _e('Ilość miejsc','kanguplugin') ?></p>
        <input class="quantities" id="quantity" type="number" name="" value="">
        <p><?php echo _e('Ilość skarpetek','kanguplugin') ?></p>
        <input class="quantities" id="socks_quantity" type="number" name="" value="">
        <p><input id="no_socks" type="checkbox" name="" value=""> <label for=""><?php _e('Nie zamawiam skarpetek','kanguplugin') ?></label> </p>
      </div>

      <div id="pricesDiv" class="col-md-12 kanguplugin_hide text-center">
        <input id="savedProductPrice" type="hidden" name="" value="<?php echo $savedProduct ?>">
        <strong><span id="totalPrice"></span> <span><?php echo $sign ?></span>  </strong>
      </div>

      <div id="submitDiv" class="col-md-12 kanguplugin_hide">
        <button id="submit" type="button" name="button"><?php _e('Rezerwuj termin','kanguplugin') ?></button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  jQuery(document).ready(function(){
    $('.<?php echo $handler ?>').on('click',function(){
      $('#app').fadeIn()
    })
  })
</script>
