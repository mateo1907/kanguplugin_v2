<script type="text/template" id="tmpl-flashMessage">

<div class=" alert alert-{{type}}">
  <# if (type == 'success') {#>
    <i class="messageIco fa fa-check-circle-o" aria-hidden="true"></i> {{message}}
  <# } else { #>
    <i class="messageIco fa fa-times-circle-o" aria-hidden="true"></i> {{message}}
  <# } #>
</div>
</script>

<script type="text/template" id="tmpl-flashMessageMultiple">

<div class=" alert alert-{{type}} multiple">
  <# if (type == 'success') {#>
    <i class="messageIco fa fa-check-circle-o" aria-hidden="true"></i>
    {{message}}
  <# } else { #>
    <i class="messageIco fa fa-times-circle-o" aria-hidden="true"></i>
    <# _.map(message, function(msg){ #>
      <p>{{msg}}</p>
    <# }) #>
  <# } #>
</div>
</script>
<script type="text/javascript">

  _.templateSettings = {
     evaluate:    /<#([\s\S]+?)#>/g,
     interpolate: /\{\{\{([\s\S]+?)\}\}\}/g,
     escape:      /\{\{([^\}]+?)\}\}(?!\})/g,
   };

  var flashMessageTemplate = _.template(jQuery('#tmpl-flashMessage').html());
  var flashMessageMultipleTemplate = _.template(jQuery('#tmpl-flashMessageMultiple').html());

</script>
