<div class="wrap">
  <h2 id="kanguplugin_title"><?php echo __($title) ?></h2> <img class="loader" src="/wp-admin/images/loading.gif" alt="">

  <h4>Dodawanie nowych dat</h4>
  <form id="actionAddDateForm">
    <input id="nonce" type="hidden" name="nonce" value="<?php echo $nonce ?>">
    <input type="radio" name="type" value="1"> <label for="">Pojedyncza data</label>
    <input type="radio" name="type" value="2"> <label for="">Zakres dat</label>
    <div style="display:none" class="type_1">
      <label for="">Wybierz datę:</label>
      <input type="text" name="date" id="date" value="">
    </div>
    <div style="display:none"  class="type_2">
      <label for="">Wybierz zakres dat:</label>
      <input type="text" name="" id="from" value="">
      <input type="text" name="" id="to" value="">
    </div>
    <!-- <div style="display:none" class="status">
      <input type="checkbox" name="status" value="1"> <label for="">Dni piątek - niedziela / święta</label>
    </div> -->
      <div class="">
        <input class='button button-primary' type="submit" name="" value="Dodaj datę / zakres dat">
      </div>
  </form>
  <hr>
  <h4>Przydział dni świątecznych</h4>
  <form>
    <div class="">
      <label for="">Wprowadź datę:</label>
      <input id="holiday" type="text" name="" value="">
    </div>
    <div class="">
      <button id="setHoliday" class="button button-primary" type="button" name="button">Dodaj dzień świąteczny</button>
    </div>
  </form>
  <a id="getHolidays" href="#">Lista dni świątecznych</a>
  <div id="holidaysDiv">

  </div>
  <hr>
  <h4>Przydział dat, godzin i grup</h4>
  <div class="form-style-8">
    <div data-id="1" class="dateHead">
      <strong>Godziny (produkty)</strong>
    </div>
    <div id="content_1" class="showOnClick">
      <div id="hours">
        <table class="zui-table zui-table-horizontal zui-table-highlight">
          <thead>
            <tr>
              <th>Godzina</th>
              <th>Ilość miejsc</th>
              <th>Wspólna ilość miejsc dla grup</th>
              <th><a data-inputs="productsCheck" class="checkInputs" href="#">Zaznaczenie</a></th>

            </tr>
          </thead>
          <tbody>
            <?php foreach ($products as $p): ?>
              <tr>
                <td><?php echo $p->post_title ?></td>
                <td><input class="productsQuantity" id="productsQuantity<?php echo $p->ID ?>" type="number" ></td>
                <td><span><?php echo $savedQuantities[$p->ID]['quantity'] ?></span>  </td>
                <td><input class="productsCheck" type="checkbox" value="<?php echo $p->ID ?>"></td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="form-style-8">
    <div data-id="2" class="dateHead">
      <p><strong>Wybór dat</strong></p>
    </div>
    <div id="content_2" class="showOnClick">
      <a id='refreshDates' href="#">Odśwież listę</a>
      <input id="deleteNonce" type="hidden" name="" value="<?php echo $deleteNonce ?>">
      <!-- <label for="">Filtruj według dni tygodnia</label> -->
      <!-- <select id="statusSelect" name="">
        <option value="0">Poniedziałek - Czwartek</option>
        <option value="1">Piątek - Niedziela / Święta</option>
      </select> -->
      <div id="dates">
        <img src="/wp-admin/images/loading.gif" alt="">
      </div>
    </div>
  </div>
  <div class="form-style-8">
    <div data-id="3" class="dateHead">
      <strong>Grupa użytkowników</strong>
    </div>
    <div id="content_3" class="showOnClick">
      <div id="groups">
        <img src="/wp-admin/images/loading.gif" alt="">
      </div>
    </div>
  </div>
  <div class="">
    <input type="hidden" id="addHourNonce" value="<?php echo $addHourNonce ?>">
    <button id="setToGroup" class="button button-primary" name="button">Przydziel</button>
  </div>
</div>

<script id="tmpl-productsTemplate" type="text/template">

    <table class="zui-table zui-table-horizontal zui-table-highlight">
      <thead>
        <tr>
          <th>Godzina</th>
          <th>Ilość miejsc</th>
          <th>Wspólna ilość miejsc dla grup</th>
          <th><a data-inputs="productsCheck" class="checkInputs" href="#">Zaznaczenie</a></th>

        </tr>
      </thead>
      <tbody>
        <# _.each(data, function(cell){#>
          <tr>
            <td>{{cell.post_title}}</td>
            <td><input class="productsQuantity" id="productsQuantity{{cell.ID}}" type="number" ></td>
            <td><input class="commonQuantity" id="commonQuantity{{cell.ID}}" type="number" ></td>
            <td><input class="productsCheck" type="checkbox" value="{{cell.ID}}"></td>
          </tr>
        <# }) #>
      </tbody>
    </table>

</script>

<script type="text/template" id="tmpl-datesTemplate">
  <table class="zui-table zui-table-horizontal zui-table-highlight">
    <thead>
      <tr>
        <th>Data</th>
        <!-- <th>Status</th> -->
        <th><a data-inputs="datesCheck" class="checkInputs" href="#">Zaznaczenie</a></th>
        <th>Usuń</th>
      </tr>
    </thead>
    <tbody>
      <# _.each(data, function(cell){#>
        <tr>
          <td>{{cell.date}}</td>

          <td><input class="datesCheck" type="checkbox" value="{{cell.id}}"></td>
          <td><i data-id="{{cell.id}}" class="deleteDateIco fa fa-times-circle-o" aria-hidden="true"></i></td>
        </tr>
      <# }) #>
    </tbody>
  </table>
</script>

<script type="text/template" id="tmpl-holidaysTemplate">
  <table class="zui-table zui-table-horizontal zui-table-highlight">
    <thead>
      <tr>

        <!-- <th>Status</th> -->
        <th>Data</th>
        <th>Usuń</th>
      </tr>
    </thead>
    <tbody>
      <# _.each(data, function(cell){#>
        <tr>
          <td>{{cell.date}}</td>
          <td><i data-id="{{cell.id}}" class="deleteHolidayIco fa fa-times-circle-o" aria-hidden="true"></i></td>
        </tr>
      <# }) #>
    </tbody>
  </table>
</script>

<script id="tmpl-groupsTemplate" type="text/template">
  <# _.each(data, function(cell) { #>
    <input class="group" data-max_quantity="{{cell.max_quantity}}"  type="radio" name="group" value="{{cell.id}}"> <label for="group">{{cell.name}}</label>
  <# }) #>
</script>
