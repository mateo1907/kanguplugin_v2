<div class="wrap">
  <h2 id="kanguplugin_title"><?php echo __($title) ?></h2> <img class="loader" src="/wp-admin/images/loading.gif" alt="">

  <form id="actionAddGroup" method="post">
    <input type="hidden" name="nonce" value="<?php echo $nonce ?>">
    <div class="">
      <label for="">Wprowadź nazwę grupy: </label> <input id="name" type="text" name="name" value="" placeholder="Nazwa Grupy">
    </div>
    <div class="">
      <label for="">Maksymalna dopuszczalna liczba osób: </label> <input id="maxQuantity" type="number" name="maxQuantity" value="" >
      <div class="">
        <label  for="">Przydział grupy do wspólnych ilości miejsc</label>
        <select id="commonQuantity" class="options" name="">
          <option value="1">Tak</option>
          <option value="2">Nie</option>
        </select>
        <p><input id="additionalOptions" type="checkbox" name="" value=""> <label for="">Dodatkowe opcje</label></p>
        <div id="additionalOptionsDiv" class="">
          <select id="option" class="" name="">
            <option value="1">Brak możliwości zmiany dopuszczalnej liczby osób podczas rezerwacji</option>
            <option value="2">Podana dopuszczalna liczba osób jest minimalną wartością</option>
          </select>

        </div>
      </div>
      <p>Jeśli liczba osób dla określonej grupy będzie równa 0, wtedy użytkownik może zarezerwować maksymalną liczbę miejsc dla danej godziny</p>
    </div>

    <input class='button button-primary' type="submit" name="" value="Dodaj grupę">

  </form>
  <hr>

  <div id="groupsTemplate">
    <img src="/wp-admin/images/loading.gif" alt="">
  </div>
</div>

<script type="text/template" id="tmpl-groupsTemplate">
  <table class="zui-table zui-table-horizontal zui-table-highlight">
    <thead>
      <tr>
        <th>Nazwa grupy</th>
        <th>Maksymalna liczba osób</th>
        <th>Opcje</th>
        <th>Utworzona</th>
        <th>Usuń</th>
      </tr>
    </thead>
    <tbody>
      <# _.each(data, function(cell){#>
        <tr>
          <td>{{cell.name}}</td>
          <td>{{cell.max_quantity}}</td>
          <td>
            <# _.each(cell.options, function(opt) { #>
              <li>{{opt.id}} - {{opt.val}}</li>
              <# console.log(opt) #>
            <# }) #>
          </td>
          <td>{{cell.created}}</td>
          <td><i data-id="{{cell.id}}" class="deleteGroupIco fa fa-times-circle-o" aria-hidden="true"></i></td>
        </tr>
      <# }) #>
    </tbody>
  </table>

  <script>
    jQuery(document).ready(function($){
      $('.deleteGroupIco').on('click',function(){
        var id = $(this).attr('data-id')
        var nonce = "<?php echo wp_create_nonce('adminActionDeleteGroup') ?>";
        $.confirm({
          title: 'Potwierdź usunięcie',
          content: 'Czy na pewno chcesz usunąć ten wpis?',
          buttons: {
              Tak: function () {
                var data = {
                  'action' : 'adminActionDeleteGroup',
                  'id' : id,
                  'nonce' : nonce
                }
                  $.ajax({
                    type:'post',
                    url:ajaxurl,
                    data:data
                  })
                  .success(function(response){
                    getAllGroups();
                    var json = $.parseJSON(response);
                    var content = flashMessageTemplate(json);
                    //
                    $('#messages').html(content)

                  })

              },
              Nie: function () {

              },

          }
      });
      })
    })
  </script>
</script>
