<div class="wrap">
  <h2 id="kanguplugin_title"><?php echo __($title) ?></h2> <img class="loader" src="/wp-admin/images/loading.gif" alt="">

  <div class="">
    <h3>Klasa elementu uruchamiający popup:</h3>
    <p><strong>Wprowadź ID elementu:</strong> <input id="handler" type="text" name="" value="<?php echo $handler ?>"> </p>
    <button id="setHandler" type="button" name="button">Akceptuj</button>
  </div>
  <div class="">
    <h3>Produkt określający skarpetki</h3>
    <select id="savedProduct" name="">
      <?php foreach ($products as $p): ?>
        <option value="<?php echo $p->ID ?>"><?php echo $p->post_title ?> [ID: <?php echo $p->ID ?>]</option>
      <?php endforeach; ?>
    </select>
    <button id="setProduct" type="button" name="button">Akceptuj</button>
    <p>Zapisana wartość: <strong><?php echo $savedProduct  ?></strong> </p>
  </div>

  <div class="">
    <h3>Przejście do strony po rezerwacji:</h3>
    <select id="redirectPage" name="">
      <?php foreach ($pages as $p): ?>
        <option value="<?php echo $p->ID ?>"><?php echo $p->post_title ?></option>
      <?php endforeach; ?>
    </select>
    <button id="setRedirectPage" type="button" name="button">Akceptuj</button>
  </div>

</div>
