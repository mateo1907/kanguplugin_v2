<div class="wrap">
  <h2 id="kanguplugin_title"><?php echo __($title) ?></h2> <img class="loader" src="/wp-admin/images/loading.gif" alt="">
  <p><strong>Dodanie ceny dla konkretnej grupy użytkowników i jego statusu (dni tygodnia / weekendów / świąt)</strong></p>
  <form id="addPrice">
    <input type="hidden" id="nonce" value="<?php echo $nonce ?>">
    <div class="">
      <label for="">Grupa użytkowników</label>
      <select id="groups">
        <?php foreach ($groups as $g): ?>
          <option value="<?php echo $g['id'] ?> "><?php echo $g['name'] ?></option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="">
      <label for="">Zakres dni</label>
      <select id="range">
        <option value="0">Poniedziałek - Czwartek</option>
        <option value="1">Piątek - Niedziela / Święta</option>
      </select>
    </div>
    <div class="">
      <label for="">Cena</label>
      <input id="price" type="text" >
    </div>
    <div class="">
      <button id="setPrice" type="submit" class="button button-primary" name="button">Zatwierdź cenę</button>
    </div>
  </form>
  <h3>Zapisane ceny</h3>
  <div id="savedPrices">
      <img src="/wp-admin/images/loading.gif" alt="">
  </div>
</div>

<script type="text/template" id="tmpl-prices">
  <table class="zui-table zui-table-horizontal zui-table-highlight">
    <input type="hidden" id="deleteNonce" value="<?php echo $deleteNonce ?>">
    <thead>
      <tr>
        <th>Grupa</th>
        <th>Cena</th>
        <th>Status</th>
        <th>Utworzona</th>
        <th>Usuń</th>
      </tr>
    </thead>
    <tbody>
      <# _.each(data, function(cell){#>
        <tr>
          <td>{{cell.name}}</td>
          <td>{{cell.price}}</td>
          <td>{{cell.status}}</td>
          <td>{{cell.created}}</td>
          <td><i data-id="{{cell.id}}" class="deletePriceIco fa fa-times-circle-o" aria-hidden="true"></i></td>
        </tr>
      <# }) #>
    </tbody>
  </table>

  <script>
  jQuery(document).ready( function($) {
    $('.deletePriceIco').on('click',function() {
      let id = $(this).attr('data-id')
      let deleteNonce = $('#deleteNonce').val()
      let data = {
        action: 'adminActionDeletePrice',
        id:id,
        deleteNonce:deleteNonce
      }
      console.log(data)
      $.confirm({
        title: 'Potwierdź usunięcie',
        content: 'Czy na pewno chcesz usunąć ten wpis?',
        buttons: {
            Tak: function () {
              $.ajax({
                method:'post',
                url:ajaxurl,
                data:data
              })
              .success( function(response) {
                let json = $.parseJSON(response)
                // console.log(json)
                let content = flashMessageTemplate(json)

                $('#messages').html(content)
                getSavedPrices()
              })
            },
            Nie: function () {

            },

        }
      });

    })
  })
  </script>
</script>
