<div class="wrap">
  <h2 id="kanguplugin_title"><?php echo __($title) ?></h2> <img class="loader" src="/wp-admin/images/loading.gif" alt="">
<div class="form-style-8">

  <div data-id="1" class="reservations_slide">
    Dodawanie nowej rezerwacji
  </div>
  <div class="reservations_slide_content" id="content_1">

    <form id="addReservation">
      <input type="hidden" id="nonce" value="<?php echo $nonce ?>">
      <div class="">
        <label for="">Imię</label>
        <input type="text" id="firstname" value="">
      </div>
      <div class="">
        <label for="">Nazwisko</label>
        <input type="text" id="lastname" value="">
      </div>
      <div class="">
        <p><label for="">Grupa użytkowników</label></p>
        <!-- <select class="" id="group"> -->
          <?php foreach ($groups as $g): ?>
            <input class="group" type="radio" name="group" data-<?php echo $g['options'][0]['id'] ?>="<?php echo $g['options'][0]['val'] ?>" data-<?php echo $g['options'][1]['id'] ?>="<?php echo $g['options'][1]['val'] ?>" value="<?php echo $g['id'] ?>">
            <label for=""><?php echo $g['name'] ?></label>
          <?php endforeach; ?>
        <!-- </select> -->
      </div>
      <div class="">
        <label for="">Wybierz datę</label>
        <input type="text" id="date" value="">
        <button type="button" id="checkHour" class="button button-primary" name="button">Sprawdź godziny dla daty</button>

      </div>
      <div id="hoursDiv"  class="showAfterHoursList">
        <label for="">Godziny dostępne dla wybranej daty i grupy:</label>
        <div class="emptyBeforeLoadHours" id="hours">

        </div>
      </div>
      <div>
        <label for="">Adres e-mail</label>
        <input type="text" id="email" value="">
      </div>
      <div class="">
        <label for="">Numer telefonu</label>
        <input type="text" id="phone" value="">
      </div>
      <div class="showAfterHourCheck">
        <label for="">Ilość miejsc</label>
        <input type="number" class="quantities emptyBeforeLoadHours emptyOnHourClick" id="quantity">

      </div>
      <div class="showAfterHourCheck">
        <label for="">Ilość skarpetek</label>
        <input type="number" class="quantities emptyBeforeLoadHours emptyOnHourClick" id="socks_quantity">

        <input type="checkbox" id="no_socks" value=""> <label for="">Klient nie zamawia skarpetek</label>
      </div>

      <button class=" button button-primary" type="submit" name="button">Dodaj rezerwację</button>
    </form>

  </div>
</div>

  <div class="form-style-8">
    <div data-id="2" class="reservations_slide">
      Podgląd rezerwacji
    </div>
    <div <?php echo $_GET['show'] == 1 ? 'style="display:block"' : '' ?> class="reservations_slide_content" id="content_2" class="">
      <input type="hidden" id="deleteNonce" value="<?php echo $deleteNonce ?>">
        <div>
          <p>Zakres dat:</p>
          <input id="dateFrom" type="text" name="" value="">
          <input id="dateTo" type="text" name="" value="">
          <button id="filtr" class="button button-primary"type="button" name="button">Filtruj</button>
        </div>

        <div id="reservationsTemplate">
          <img style="display:block;margin:0 auto"  src="/wp-admin/images/loading.gif" alt="">
        </div>
    </div>
  </div>
</div>


<script type="text/template" id="tmpl-reservations">

<table style="width:100%" class="zui-table zui-table-horizontal zui-table-highlight">
  <thead>
    <th>ID</th>
    <th>Imię</th>
    <th>Nazwisko</th>
    <th>Grupa użytkowników</th>
    <th>Telefon</th>
    <th>Email</th>
    <th>Godzina</th>
    <th>Ilość</th>
    <th>Wartość zamówienia</th>
    <th>Skarpetki</th>
    <th>Ilość skarpetek</th>
    <th>Utworzona</th>
    <th>Usuń</th>
  </thead>
  <tbody>
    <# _.each(data, function(cell){ #>
      <tr>
        <td>{{cell.id}}</td>
        <td>{{cell.firstname}}</td>
        <td>{{cell.lastname}}</td>
        <td>{{cell.name}}</td>
        <td>{{cell.phone}}</td>
        <td>{{cell.email}}</td>
        <td>{{cell.id_product}}</td>
        <td>{{cell.quantity}}</td>
        <td>{{cell.price}}</td>
        <td>{{cell.socks}}</td>
        <td>{{cell.socks_quantity}}</td>
        <td>{{cell.created}}</td>
        <td><i data-id="{{cell.id}}" class="deleteReservationIco fa fa-times-circle-o" aria-hidden="true"></td>
      </tr>
    <# }) #>
  </tbody>

</table>
</script>
