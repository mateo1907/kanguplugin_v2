<div class="wrap">
  <h2 id="kanguplugin_title"><?php echo __($title) ?></h2> <img class="loader" src="/wp-admin/images/loading.gif" alt="">
  <div class="">
    <input id="nonce" type="hidden" name="" value="<?php echo $nonce ?>">
    <table style="width:100%" class="zui-table zui-table-horizontal zui-table-highlight">
      <thead>
        <th>Godzina</th>
        <th>Wspólna ilość miejsc</th>
      </thead>
      <tbody>
        <?php foreach ($products as $p): ?>
          <tr>
            <td><?php echo $p->post_title ?></td>
            <td><input data-id="<?php echo $p->ID ?>" class="commonQuantity" type="text" name="" value="<?php echo $savedQuantities[$p->ID]['quantity'] ?>"> </td>

          </tr>

        <?php endforeach; ?>
      </tbody>
    </table>
    <input id="setCommonQuantities" type="button" name="" value="Zapisz">

  </div>
</div>
