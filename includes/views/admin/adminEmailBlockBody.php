<div class="wrap">

  <h2 id="kanguplugin_title"><?php echo __($title) ?></h2> <img class="loader" src="/wp-admin/images/loading.gif" alt="">
  <form class=""  method="post">
    <?php the_editor($content) ?>
    <button id="setEmailTemplate" class="button button-primary" type="submit" name="button">Zapisz</button>
  </form>

  <h3>Zmienne możliwe do wykorzystania w szablonie</h3>
  <p><strong>{{firstname}}</strong> - Imię </p>
  <p><strong>{{lastname}}</strong> - Nazwisko </p>
  <p><strong>{{phone}}</strong> - Telefon </p>
  <p><strong>{{email}}</strong> - Adres e-mail </p>
  <p><strong>{{quantity}}</strong> - Ilość zamówionych miejsc </p>
  <p><strong>{{socks_quantity}}</strong> - Ilość par skarpetek </p>
  <p><strong>{{hour}}</strong> - Wybrana godzina </p>
  <p><strong>{{group}}</strong> - Grupa użytkowników </p>
  <p><strong>{{total_price}}</strong> - Ogólna wartość zamówienia </p>

</div>
