<?php

namespace KanguPlugin;

class Holidays extends Model
{
  protected $id;
  protected $date;
  protected $wpdb;
  protected $holidayObj;
  protected $error;
  protected $table = 'kangu_holidays';
  protected $fillable = ['date'];

  public function __construct($id = null)
  {
    global $wpdb;
    $this->wpdb = $wpdb;

    if ($id) {
      $this->id = $id;
      $this->holidayObj = $this->wpdb->get_row("SELECT * FROM {$this->getFullTableName()} WHERE id = {$this->id}");
    }
  }

  public function getFullTableName()
  {
    return $this->wpdb->prefix . $this->table;
  }

  public function getHolidays($array = null)
  {
    $q = $this->wpdb->get_results("SELECT * FROM {$this->getFullTableName()} WHERE 1",ARRAY_A);
    $data = array();

    foreach ($q as $h) {
      $data[] = $q['date'];
    }
    if ($array) {
      return $q;
    } else {
      return $data;
    }
  }
    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Date
     *
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of Date
     *
     * @param mixed date
     *
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }


    /**
     * Get the value of Wpdb
     *
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set the value of Wpdb
     *
     * @param mixed wpdb
     *
     * @return self
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * Get the value of Holiday Obj
     *
     * @return mixed
     */
    public function getHolidayObj()
    {
        return $this->holidayObj;
    }

    /**
     * Set the value of Holiday Obj
     *
     * @param mixed holidayObj
     *
     * @return self
     */
    public function setHolidayObj($holidayObj)
    {
        $this->holidayObj = $holidayObj;

        return $this;
    }



}
