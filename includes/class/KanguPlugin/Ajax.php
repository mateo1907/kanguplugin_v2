<?php
namespace KanguPlugin;
/**
 * Klasa zawierająca metody obsługujące Ajax
 */
use KanguPlugin\JSON;
use KanguPlugin\Price;
class Ajax
{

  protected $functions;

  public function __construct()
  {
    $this->functions = get_class_methods($this);
  }

  /**
   * Zainicjowanie wszystkich metod ajaxowych dla Admina
   */

  public function initAdmin()
  {
    foreach ($this->functions as $function) {

      if (strpos($function, 'adminAction') !== false) {

        add_action('wp_ajax_'.$function,array($this,$function));
        add_action('wp_ajax_nopriv_'.$function,array($this,$function));
      }

    }
  }

  /**
   * Inicjowanie calosc Ajax front
   * @return [type] [description]
   */
  public function initFront()
  {
    foreach ($this->functions as $function) {
      if (strpos($function, 'frontAction') !== false) {
        // var_dump($function);
        add_action('wp_ajax_'.$function,array($this,$function));
        add_action('wp_ajax_nopriv_'.$function,array($this,$function));
      }

    }
  }


  /**
   * Dodanie grupy
   * @return [type] [description]
   */
  public function adminActionAddGroup()
  {
    if (wp_verify_nonce($_POST['nonce'], 'actionAddGroup')) {
      $name = sanitize_text_field($_POST['name']);

      $group = new Group;
      $group->setName($name);
      $group->setMaxQuantity((int)$_POST['maxQuantity']);
      $group->setOptions(serialize($_POST['options']));
      $group->setCreated(date('Y-m-d H:i:s'));

      if ($group->save()) {
        echo JSON::encode(['type'=>'success','message'=>'Dodano pomyślnie']);
      } else {
        echo JSON::encode(['type'=>'danger','message'=>$group->getError() ]);
      }


      wp_die();
    }
  }

  /**
   * Wyswietlenie wszystkich grup
   * @return [type] [description]
   */
  public function adminActionShowGroups()
  {
    $groups = new Group();
    echo JSON::encode(['data' => $groups->getGroups()]) ;
    wp_die();
  }

  /**
   * usunięcie pojedynczej grupy
   */

  public function adminActionDeleteGroup()
  {
    if (wp_verify_nonce($_POST['nonce'],'adminActionDeleteGroup')) {
      $group = new Group((int)$_POST['id']);
      if ($group->delete()) {
        echo JSON::encode(['type'=>'success','message'=>'Usunięto pomyślnie']);
      } else {
        echo JSON::encode(['type'=>'danger','message'=>$group->getError() ]);
      }
      wp_die();
    }
  }

  /**
   * Daty - dodanie
   */

  public function adminActionAddDate()
  {
    if (wp_verify_nonce($_POST['nonce'],'actionAddDate')) {
      if (is_array($_POST['date'])) {
        foreach ($_POST['date'] as $d) {

          $date = new Date();
          if (!$date->checkExist($d)) {
            echo JSON::encode(['type'=>'danger','message'=>$date->getError() ]);
            wp_die();
          }
          $date->setDate(sanitize_text_field($d));
          // $date->setStatus((int)$_POST['status']);
          $date->setActive(1);
          $date->setCreated(date('Y-m-d H:i:s'));

          if (!$date->save()) {
            echo JSON::encode(['type'=>'danger','message'=>$date->getError() ]);
            wp_die();
          }
        }
      } else {
        $date = new Date();
        if (!$date->checkExist($_POST['date'])) {
          echo JSON::encode(['type'=>'danger','message'=>$date->getError() ]);
          wp_die();
        }
        $date->setDate(sanitize_text_field($_POST['date']));
        $date->setStatus((int)$_POST['status']);
        $date->setActive(1);
        $date->setCreated(date('Y-m-d H:i:s'));

        if (!$date->save()) {
          echo JSON::encode(['type'=>'danger','message'=>$date->getError() ]);
          wp_die();
        }
      }
      echo JSON::encode(['type'=>'success','message'=>'Dodano pomyślnie daty']);
      wp_die();
    }
  }

  /**
   * Wyświetlenie wszystkich produktów na podstronie z datą
   */
  public function adminActionShowKanguProducts()
  {
    $products = Products::getAllKanguProducts();

    echo JSON::encode(['data' => $products]);
    wp_die();
  }

  /**
   * Wyświetlenie wszystkich dat
   */
  public function adminActionGetAllDates()
  {

      $dates = new Date();
      $allDates = $dates->getAllDates();
      echo JSON::encode(['data' => $allDates]);
      wp_die();
  }

  /**
   * Usunięcie daty
   */

  public function adminActionDeleteDate()
  {
    if (wp_verify_nonce($_POST['nonce'],'deleteNonce')) {
      $date = new Date((int)$_POST['id']);
      if ($date->delete()) {
        echo JSON::encode(['type'=>'success','message'=>'Usunięto pomyślnie']);
      } else {
        echo JSON::encode(['type'=>'danger','message'=>$date->getError() ]);
      }
      wp_die();
    }
  }

  /**
   * Pobranie dni świątecznych
   */
   public function adminActionGetAllHolidays()
   {
     $holiday = new Holidays;
     $data = $holiday->getHolidays(true);

     echo JSON::encode(['data'=>$data]);
     wp_die();
   }

   /**
    * Usunięcie dnia świątecznego
    */

   public function adminActionDeleteHoliday()
   {
     if (wp_verify_nonce($_POST['nonce'],'deleteNonce')) {
       $holiday = new Holidays((int)$_POST['id']);
       if ($holiday->delete()) {
         echo JSON::encode(['type'=>'success','message'=>'Usunięto pomyślnie']);
       } else {
         echo JSON::encode(['type'=>'danger','message'=>$holiday->getError() ]);
       }
       wp_die();
     }
   }

  /**
   * Zapis godzin do bazy
   */

  public function adminActionSaveHour()
  {
    if (wp_verify_nonce($_POST['nonce'],'actionAddHour')) {
      $errors = [];
      foreach ($_POST['products'] as $p) {

        foreach ($_POST['dates'] as $d) {

          $hour = new Hours();
          $hour->setIdDate((int)$d);
          $hour->setIdProduct((int)$p['product']);
          $hour->setQuantity((int)$p['quantity']);
          // $hour->setCommonQuantity((int)$p['commonQuantity']);
          $hour->setIdGroup((int)$_POST['group']);
          $hour->setCreated(date('Y-m-d H:i:s'));

          if ($hour->checkExist($hour)) {
            if (!$hour->save()) {
              $errors[] = $hour->getError();
            }
          } else {
            $errors[] = $hour->getError();
          }

        }

      }
      if (empty($errors)) {
        echo JSON::encode(['type'=>'success','message'=>'Dodano pomyślnie']);
      } else {
        echo JSON::encode(['type'=>'danger','message'=>$errors ]);
      }
      wp_die();
    }
  }

  /**
   * Zapis ceny
   */
  public function adminActionAddPrice()
  {
    if (wp_verify_nonce($_POST['nonce'],'actionAddPrice')) {
      // if (!$_POST['price']) {
      //
      //   echo JSON::encode(['type'=>'danger','message'=>'Cena nie może pozostać pusta']);
      //   wp_die();
      // }
      $price = new Prices();
      $price->setIdGroup((int)$_POST['group']);
      $price->setStatus((int)$_POST['range']);
      $price->setPrice((float)$_POST['price']);
      $price->setCreated(date('Y-m-d H:i:s'));
      if ($price->checkExist((int)$price->getIdGroup(),(int)$price->getStatus())) {
        if ($price->save()) {
          echo JSON::encode(['type'=>'success','message'=>'Wszystko w porządku']);

        } else {
          echo JSON::encode(['type'=>'danger','message'=>$price->getError()]);
        }
      } else {
        echo JSON::encode(['type'=>'danger','message'=>$price->getError()]);
      }

      wp_die();
    }
  }

  /**
   * Wyświetlenie bieżących cen
   */
  public function adminActionShowPrices()
  {
    $prices = new Prices();

    echo JSON::encode(['data'=>$prices->getPrices()]);
    wp_die();
  }

  /**
   * Usunięcie pojedynczej ceny
   */
  public function adminActionDeletePrice()
  {
    if (wp_verify_nonce($_POST['deleteNonce'],'actionDeletePrice')) {
      $price = new Prices((int)$_POST['id']);
      if ($price->delete()) {
        echo JSON::encode(['type'=>'success','message'=>'Pomyślnie usunięto']);
      } else {
        echo JSON::encode(['type'=>'danger','message'=>$price->getError()]);
      }
      wp_die();
    }
  }

  /**
   * Pobranie godzin dla daty i grupy
   */
  public function adminActionGetHoursByDateAndGroup()
  {
    if ($_GET['date']) {
      $hours = new Hours;
      $data = $hours->getHoursByDate($_GET['date'],(int)$_GET['group']);

      if (strtotime($_GET['date']) <= strtotime(date('Y-m-d'))) {
        foreach ($data as $key => $hour) {

          $currentHour = strtotime(date('H:i'));
          $hourTimestamp = strtotime($hour['title']);
          if ( $currentHour > $hourTimestamp) {
            unset($data[$key]);
          }

        }
      }



      if (!empty($data)) {
        echo JSON::encode(['type'=>'success','data'=>$data]);
      } else {
        $msg = __('Dla wybranej daty i grupy nie ma przydzielonych godzin','kanguplugin');
        echo JSON::encode(['type'=>'danger','message'=> $msg]);
      }
    } else {
      $msg = __('Wprowadź poprawną datę','kanguplugin');
      echo JSON::encode(['type'=>'danger','message'=>$msg]);
    }
    wp_die();
  }

  /**
   * Zapisanie rezerwacji do bazy danych i odjęcie wolnych miejsc
   */

  public function adminActionSaveReservation()
  {
    if (wp_verify_nonce($_POST['nonce'],'addReservation')) {
      if ($_POST['socks'] == 'false') {
        $socks = false;
        $socks_quantity = (int)0;
      } else {
        $socks = true;
        $socks_quantity = (int)$_POST['socks_quantity'];
      }
      if (!$_POST['firstname'] || !$_POST['lastname'] || !$_POST['firstname'] || !$_POST['group'] || !$_POST['hour'] || !$_POST['email'] || !$_POST['phone'] || !$_POST['quantity'] || !is_int($socks_quantity)  ) {
        echo JSON::encode(['type'=>'danger','message'=>'Uzupełnij wszystkie pola']);
        wp_die();
      }

      if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        echo JSON::encode(['type'=>'danger','message'=>'Niepoprawny adres e-mail']);
        wp_die();
      }

      // if (!is_int($_POST['group']) || !is_int($_POST['hour'])) {
      //   echo JSON::encode(['type'=>'danger','message'=>'Niepoprawne wartości ID dla Grupy lub Pola']);
      //   wp_die();
      // }

      // if (!is_int($_POST['quantity']) || !is_int($_POST['socks_quantity'])) {
      //   echo JSON::encode(['type'=>'danger','message'=>'Ilość miejsc oraz ilość skarpetek muszą być liczbą ']);
      //   wp_die();
      // }

      $reservation = new Reservations;
      $reservation->setIdGroup((int)$_POST['group']);
      $reservation->setIdHour((int)$_POST['hour']);
      $reservation->setFirstname(sanitize_text_field($_POST['firstname']));
      $reservation->setLastName(sanitize_text_field($_POST['lastname']));
      $reservation->setPhone(sanitize_text_field($_POST['phone']));
      $reservation->setEmail($_POST['email']);
      $reservation->setQuantity((int)$_POST['quantity']);
      $reservation->setSocks((bool)$socks);
      $reservation->setSocksQuantity((int)$_POST['socks_quantity']);
      $reservation->setCreated(date('Y-m-d H:i:s'));


      if ($reservation->save()) {
        $hour = new Hours((int)$_POST['hour']);
        $q = (int)$hour->getHourObj()->quantity - (int)$_POST['quantity'];
        $hour->setQuantity($q);
        $hour->update();

        echo JSON::encode(['type'=>'success','message'=>'Dodano rezerwację']);
        wp_die();
      } else {
        echo JSON::encode(['type'=>'danger','message'=>'Błąd']);
        wp_die();
      }

      }


    }

    /**
     * Pobranie wszystkich rezerwacji
     * @return [type] [description]
     */
    public function adminActionGetAllReservations()
    {
        $reservations = new Reservations;
        // var_dump($reservations->getAllReservations());
        echo JSON::encode(['data'=>$reservations->getAllReservations()]);
        wp_die();
    }

    /**
     * Pobranie rezerwacji z filtrami wyszukiwania
     */
    public function adminActionGetReservationsByFilters()
    {
      $from = sanitize_text_field($_GET['from']);
      $to = sanitize_text_field($_GET['to']);

      $reservations = new Reservations;
      // var_dump($reservations->getAllReservations());
      echo JSON::encode(['data'=>$reservations->getAllReservations($from,$to)]);
      wp_die();
    }

    /**
     * Usunięcie rezerwacji
     */
    public function adminActionDeleteReservation()
    {
      if (wp_verify_nonce($_POST['nonce'],'deleteReservation')) {
        $reservation = new Reservations((int)$_POST['id']);
        if ($reservation->delete()) {
          echo JSON::encode(['type'=>'success','message'=>'Usunięto pomyślnie']);
        } else {
          echo JSON::encode(['type'=>'danger','message'=>$group->getError() ]);
        }
        wp_die();
      }
    }

    /**
     * Ustawienie strefy czasowej w przypadku gdy nie jest ustawiona [nieuzywane na ta chwile]
     */

    public function adminActionSetHandler()
    {


      Configuration::set('handler',$_POST['handler']);

      echo JSON::encode(['type'=>'success','message'=>'Wszystko w porządku']);
      wp_die();

    }

    public function adminActionSetSavedProduct()
    {
      Configuration::set('savedProduct',(int)$_POST['savedProduct']);
      echo JSON::encode(['type'=>'success','message'=>'Wszystko w porządku']);
      wp_die();
    }

    public function adminActionSetRedirectPage()
    {
      Configuration::set('redirectPage',(int)$_POST['savedProduct']);
      echo JSON::encode(['type'=>'success','message'=>'Wszystko w porządku']);
      wp_die();
    }

    /**
     * Dodanie nowego dnia swiatecznego
     */
    public function adminActionAddHoliday()
    {
      $holiday = new Holidays;
      $holiday->setDate(sanitize_text_field($_POST['holiday']));
      if ($holiday->save()) {
        $msg = __('Pomyślnie dodano','kanguplugin');
        echo JSON::encode(['type'=>'success','message'=>$msg]);
      } else {
        $msg = __($holiday->getError(),'kanguplugin');
        echo JSON::encode(['type'=>'danger','message'=>$msg]);
      }
      wp_die();
    }

    /**
     * Pobranie ceny dla odpowiedniej grupy i statusu daty
     */

    public function frontActionGetPriceByGroupAndStatusAndGetCommonQuantity()
    {
      // var_dump($_GET);
      $hour = new Hours((int)$_GET['hour_id']);
      $product_id = $hour->getHourObj()->id_product;
      $date = $_GET['date'];
      $arr = array();
      $quantityDate = new QuantitiesDates;
      if (!$quantityDate->getBy($date,$product_id)) {
        $commonQuantity = new CommonQuantities;
        $commonQuantity->getByIdProduct($product_id);
        $quantityDate->setDate($date);
        $quantityDate->setIdCommonQuantity($commonQuantity->getId());
        $quantityDate->setQuantity($commonQuantity->getQuantity());
        $quantityDate->save();
        $arr['quantities'] = $quantityDate->getBy($date,$product_id);
      } else {
        $arr['quantities'] = $quantityDate->getBy($date,$product_id);
      }

      $price = new Prices;
      $return = $price->getPriceBy((int)$_GET['group'],(int)$_GET['status']);
      $arr['prices'] = $return;
      // var_dump($return);
      if (!empty($arr)) {
        // var_dump($arr);
          echo JSON::encode(['type'=>'success','data'=>$arr]);
      } else {
        $msg = __('Brak ceny dla wybranych kryteriów','kanguplugin');
          echo JSON::encode(['type'=>'danger','message'=>$msg]);
      }

      wp_die();

    }

    /**
     * Dodanie produktów do koszyka, zmiana ceny na przydzieloną dla odpowiedniej grupy i Daty
     */

    public function frontActionAddToCartAndChangePrice()
    {


      if (wp_verify_nonce($_POST['nonce'],'addToCart')) {
        // var_dump($_POST);

        global $woocommerce;
        $woocommerce->cart->empty_cart();
        $hourIdProduct = (new Hours((int)$_POST['hour']))->getHourObj()->id_product;
        $price = new Prices;
        $productPrice = $price->getPriceBy((int)$_POST['group'],(int)$_POST['status']);
        $currentPrice = get_post_meta($hourIdProduct,'_price',true);
        // update_post_meta($hourIdProduct,'_price',$productPrice);
        $_SESSION['price'] = $productPrice;
        $_SESSION['commonQuantity'] = (int)$_POST['commonQuantity'];
        $_SESSION['group'] = (int)$_POST['group'];
        $_SESSION['hour'] = (int)$_POST['hour'];
        $_SESSION['date'] = $_POST['date'];
        if ($_POST['option'] == 1) {
          $woocommerce->cart->add_to_cart((int)$hourIdProduct,1);
        } elseif ($_POST['option'] == 2) {
          $woocommerce->cart->add_to_cart((int)$hourIdProduct,(int)$_POST['quantity']);
        } else {
          $woocommerce->cart->add_to_cart((int)$hourIdProduct,(int)$_POST['quantity']);
        }

        if ($_POST['socks']) {
          $_SESSION['socks'] = 1;
          $woocommerce->cart->add_to_cart((int)Configuration::get('savedProduct'),(int)$_POST['socks_quantity']);
        } else {
          $_SESSION['socks'] = 0;
        }

        if (Configuration::get('redirectPage')) {
          $page = get_permalink((int)Configuration::get('redirectPage'));
          echo JSON::encode(['msg' => $page ]);
        }
        wp_die();
      }
    }

    /**
     * Dodanie wspolnych cen
     */
    public function adminActionSetCommonQuantities()
    {
      if (wp_verify_nonce($_POST['nonce'],'addCommonQuantities')) {
        foreach ($_POST['data'] as $p) {
          $commonQuantity = new CommonQuantities;
          $commonQuantity->setIdProduct((int)$p['id']);
          $commonQuantity->setQuantity((int)$p['quantity']);
          if ($commonQuantity->check()) {

            if ($commonQuantity->save()) {
              echo JSON::encode(['type'=>'success','message'=>'Dodano pomyślnie']);
            } else {
              echo JSON::encode(['type'=>'success','message'=>'Błąd']);
            }
          } else {
            $update = new CommonQuantities;
            $update->getByIdProduct((int)$p['id']);
            $update->setQuantity((int)$p['quantity']);
            if ($update->update()) {
              echo JSON::encode(['type'=>'success','message'=>'Wykonano pomyślnie']);
            } else {
              echo JSON::encode(['type'=>'success','message'=>'Błąd']);
            }
          }
        }
        wp_die();
      }
    }

    public function adminActionGetQuantitiesByDate()
    {
      $hour = new Hours((int)$_GET['id']);
      $product_id = $hour->getHourObj()->id_product;
      $date = $_GET['date'];
      $arr = array();
      $quantityDate = new QuantitiesDates;
      if (!$quantityDate->getBy($date,$product_id)) {
        $commonQuantity = new CommonQuantities;
        $commonQuantity->getByIdProduct($product_id);
        $quantityDate->setDate($date);
        $quantityDate->setIdCommonQuantity($commonQuantity->getId());
        $quantityDate->setQuantity($commonQuantity->getQuantity());
        $quantityDate->save();
        echo JSON::encode(['data'=>$quantityDate->getBy($date,$product_id)]);
      } else {
        echo JSON::encode(['data'=>$quantityDate->getBy($date,$product_id)]);
      }
      wp_die();
    }





}
