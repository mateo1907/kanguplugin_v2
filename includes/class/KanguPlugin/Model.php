<?php
namespace KanguPlugin;

class Model
{
  public function save()
  {
    try {
      $args = array();
      foreach ($this->fillable as $var) {
        if ($this->$var) {

            $args[$var] = $this->$var ;

        }
      }

      $return = $this->wpdb->insert(
       $this->getFullTableName(),
       $args
      );

      return $return;


    } catch (\Exception $e) {

      return false;
    }

  }

  public function update()
  {
    try {

      if (!$this->id || !is_int($this->id)) {
        throw new \Exception("Błąd identyfikatora");

      }

      $args = array();
      foreach ($this->fillable as $var) {
        if ($this->$var || $this->$var !== NULL) {

            $args[$var] = $this->$var ;

        }
      }
      $this->wpdb->update(
        $this->getFullTableName(),
        $args,
        array( 'ID' => $this->id )
      );

      return true;
    } catch (\Exception $e) {


      return false;
    }

  }

  public function delete()
  {

    try {
      if (!$this->id || !is_int($this->id)) {
        throw new \Exception("Błąd identyfikatora");

      }

      $this->wpdb->delete($this->getFullTableName(), array('id' => $this->id));
      return true;

    } catch (\Exception $e) {
      return false;
    }


  }

  public function findBy($name,$value)
  {
    $q = $this->wpdb->get_results("SELECT * FROM {$this->getFullTableName()} WHERE $name = '{$value}'",ARRAY_A);

    return $q;
  }
}
