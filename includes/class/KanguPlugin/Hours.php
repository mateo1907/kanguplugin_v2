<?php
namespace KanguPlugin;

/**
 * Klasa obsługująca godziny
 */

class Hours extends Model
{

  protected $id;
  protected $id_product;
  protected $id_group;
  protected $id_date;

  protected $quantity;
  protected $commonQuantity;
  protected $created;

  protected $hourObj;
  protected $error;
  protected $table = 'kangu_hours';
  protected $wpdb;

  protected $fillable = ['id_product','id_group','id_date','quantity','commonQuantity','created'];

  public function __construct($id = null)
  {
    global $wpdb;

    $this->wpdb = $wpdb;

    if ($id) {
      $this->id = $id;
      $this->hourObj = $this->wpdb->get_row("SELECT * FROM {$this->getFullTableName()} WHERE id = {$this->id}");
    }
  }


    /**
     * Get the value of Klasa obsługująca godziny
     *
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set the value of Klasa obsługująca godziny
     *
     * @param mixed id
     *
     * @return self
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * Get the value of Id Product
     *
     * @return mixed
     */
    public function getIdProduct()
    {
        return $this->id_product;
    }

    /**
     * Set the value of Id Product
     *
     * @param mixed id_product
     *
     * @return self
     */
    public function setIdProduct($id_product)
    {
        $this->id_product = $id_product;

        return $this;
    }

    public function getHourObj()
    {
      return $this->hourObj;
    }
    /**
     * Get the value of Id Group
     *
     * @return mixed
     */
    public function getIdGroup()
    {
        return $this->id_group;
    }

    /**
     * Set the value of Id Group
     *
     * @param mixed id_group
     *
     * @return self
     */
    public function setIdGroup($id_group)
    {
        $this->id_group = $id_group;

        return $this;
    }

    /**
     * Get the value of Id Date
     *
     * @return mixed
     */
    public function getIdDate()
    {
        return $this->id_date;
    }

    /**
     * Set the value of Id Date
     *
     * @param mixed id_date
     *
     * @return self
     */
    public function setIdDate($id_date)
    {
        $this->id_date = $id_date;

        return $this;
    }



    /**
     * Get the value of Quantity
     *
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set the value of Quantity
     *
     * @param mixed quantity
     *
     * @return self
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get the value of Quantity
     *
     * @return mixed
     */
    public function getCommonQuantity()
    {
        return $this->commonQuantity;
    }

    /**
     * Set the value of Quantity
     *
     * @param mixed quantity
     *
     * @return self
     */
    public function setCommonQuantity($commonQuantity)
    {
        $this->commonQuantity = $commonQuantity;

        return $this;
    }

    /**
     * Get the value of Created
     *
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set the value of Created
     *
     * @param mixed created
     *
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    public function getFullTableName()
    {
      return $this->wpdb->prefix . $this->table;
    }



    public function checkExist(Hours $hour)
    {
      $q = $this->wpdb->get_var("SELECT COUNT(*) FROM {$this->getFullTableName()} WHERE id_product = {$hour->getIdProduct()} AND id_group = {$hour->getIdGroup()} AND id_date = {$hour->getIdDate()}");

      if ((int)$q > 0) {
        $this->setError("Rekord zawierający dane [ID_DNIA: {$hour->getIdDate()}, ID_GRUPY: {$hour->getIdGroup()}, ID_PRODUKT:{$hour->getIdProduct()}] już istnieje w bazie");
        return false;
      }
      return true;
    }

    public function getHoursByDate($date,$group)
    {
      $dateTable = Configuration::getTableName(Date::class);
      $commonQuantity = new QuantitiesDates;
      $savedQuantities = $commonQuantity->getBy($date);
      $holidays = new Holidays;
      $holidaysArr = $holidays->getHolidays();
      $workDays = [1,2,3,4];
      $holidayDays = [5,6,7];
      $day_of_week = date('N', strtotime($date));

      if (in_array($date, $holidaysArr)) {
        $status = 1;
      } elseif (in_array($day_of_week, $holidayDays)) {
        $status = 1;
      } else {
        $status = 0;
      }







      $q = $this->wpdb->get_results("SELECT hours.id, hours.quantity, hours.id_product FROM {$this->getFullTableName()} hours INNER JOIN {$dateTable} dates on hours.id_date = dates.id WHERE dates.date = '{$date}' AND hours.id_group = {$group} AND dates.active = 1" );
      $returnArray = array();

      foreach ($q as $obj) {
        if ($obj->quantity > 0) {
          $product = wc_get_product($obj->id_product);
          $returnArray[] = array(
            'id' => $obj->id,
            'title' => $product->get_title(),
            'quantity' => $obj->quantity,
            'dateStatus' => $status,
            // 'commonQuantity' => $savedQuantities[$obj->id_product]['quantity']

          );
        }
      }

      usort($returnArray, function($a,$b){
         return (strtotime($a['title']) > strtotime($b['title']));
      });
      return $returnArray;
    }

}
