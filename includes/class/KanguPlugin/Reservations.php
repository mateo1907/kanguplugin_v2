<?php

namespace KanguPlugin;

/**
 * Klasa obsługująca rezerwacje
 */

class Reservations extends Model
{
  protected $id;
  protected $id_group;
  protected $id_hour;
  protected $firstname;
  protected $lastname;
  protected $phone;
  protected $email;
  protected $quantity;
  protected $price;
  protected $socks;
  protected $socks_quantity;
  protected $created;
  protected $reservationObj;
  protected $fillable = ['id_group','id_hour','firstname','lastname','phone','email','quantity','socks','socks_quantity','price','created'];
  protected $table = 'kangu_reservations';
  protected $wpdb;

  public function __construct($id = null)
  {
    global $wpdb;
    $this->wpdb = $wpdb;

    if ($id) {
      $this->id = $id;
      $this->reservationObj = $this->wpdb->get_row("SELECT * FROM {$this->getFullTableName()} WHERE id = {$this->id}");
    }
  }



    /**
     * Get the value of Id Group
     *
     * @return mixed
     */
    public function getIdGroup()
    {
        return $this->id_group;
    }

    /**
     * Set the value of Id Group
     *
     * @param mixed id_group
     *
     * @return self
     */
    public function setIdGroup($id_group)
    {
        $this->id_group = $id_group;

        return $this;
    }

    /**
     * Get the value of Id Group
     *
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set the value of Id Group
     *
     * @param mixed id_group
     *
     * @return self
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice()
    {
      return $this->price;
    }

    public function setPrice($price)
    {
      $this->price = $price;
      return $this;
    }


    public function getReservationObj()
    {
      return $this->reservationObj;
    }

    /**
     * Get the value of Firstname
     *
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set the value of Firstname
     *
     * @param mixed firstname
     *
     * @return self
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get the value of Lastname
     *
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set the value of Lastname
     *
     * @param mixed lastname
     *
     * @return self
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get the value of Phone
     *
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set the value of Phone
     *
     * @param mixed phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get the value of Email
     *
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of Email
     *
     * @param mixed email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of Socks
     *
     * @return mixed
     */
    public function getSocks()
    {
        return $this->socks;
    }

    /**
     * Set the value of Socks
     *
     * @param mixed socks
     *
     * @return self
     */
    public function setSocks($socks)
    {
        $this->socks = $socks;

        return $this;
    }

    /**
     * Get the value of Socks Quantity
     *
     * @return mixed
     */
    public function getSocksQuantity()
    {
        return $this->socks_quantity;
    }

    /**
     * Set the value of Socks Quantity
     *
     * @param mixed socks_quantity
     *
     * @return self
     */
    public function setSocksQuantity($socks_quantity)
    {
        $this->socks_quantity = $socks_quantity;

        return $this;
    }

    /**
     * Get the value of Created
     *
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set the value of Created
     *
     * @param mixed created
     *
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get the value of Id Hour
     *
     * @return mixed
     */
    public function getIdHour()
    {
        return $this->id_hour;
    }

    /**
     * Set the value of Id Hour
     *
     * @param mixed id_hour
     *
     * @return self
     */
    public function setIdHour($id_hour)
    {
        $this->id_hour = $id_hour;

        return $this;
    }

    public function getFullTableName()
    {
      return $this->wpdb->prefix . $this->table;
    }


    /**
     * Pobranie wszystkich rezerwacji
     */

    public function getAllReservations($from = null,$to = null)
    {
      $groupTable = Configuration::getTableName(Group::class);
      $hourTable = Configuration::getTableName(Hours::class);

      if (!$from && !$to) {
        $q = $this->wpdb->get_results("SELECT reservations.id, reservations.firstname, reservations.lastname, reservations.phone, reservations.email, groups.name, hours.id_product, reservations.quantity, reservations.price, reservations.socks, reservations.socks_quantity,reservations.created FROM {$this->getFullTableName()} reservations INNER JOIN {$groupTable} groups ON reservations.id_group = groups.id INNER JOIN {$hourTable} hours ON reservations.id_hour = hours.id WHERE 1",ARRAY_A);
      } else {
        $q = $this->wpdb->get_results("SELECT reservations.id, reservations.firstname, reservations.lastname, reservations.phone, reservations.email, groups.name, hours.id_product, reservations.quantity, reservations.price, reservations.socks, reservations.socks_quantity,reservations.created FROM {$this->getFullTableName()} reservations INNER JOIN {$groupTable} groups ON reservations.id_group = groups.id INNER JOIN {$hourTable} hours ON reservations.id_hour = hours.id WHERE (reservations.created BETWEEN '{$from}' AND '{$to}')",ARRAY_A);
      }


      $returnArray = array();
      foreach ($q as $k => $subArr) {
        $subArr['id_product'] = get_the_title($subArr['id_product']);
        $subArr['socks'] = $subArr['socks'] == 1 ? 'Tak' : 'Nie';
        $returnArray[] = $subArr;

      }

      return $returnArray;
    }





}
