<?php
namespace KanguPlugin;
use KanguPlugin\Ajax;
use \WC_Order;
class Front
{
  /**
   * Inicjalizacja Frontu
   */

  public function init()
  {

    $args = array(
      'quantity' => __('Wartość nie może być większa niż ','kanguplugin'),
    );
    $conf = new Configuration;
    // $conf->addAjaxurl();
    $conf->addAdminCSS('front');
    $conf->addFrontJS('front',$args);



    add_shortcode('kangujump_reservation',array($this,'frontActionReservationPage'));

    add_action('wp_footer',function(){
      echo '<script type="text/javascript">
             var ajaxurl = "' . admin_url('admin-ajax.php') . '";
           </script>';
    });
    add_action('wp_footer',function(){
      echo do_shortcode('[kangujump_reservation]');
    });

    add_action('woocommerce_checkout_order_processed',array($this,'frontDoReservation'));
  }

  /**
   * Metoda wywołująca widok dla Frontu
   */

  public function frontActionReservationPage()
  {


    $groups = new Group;
    $today = date('Y-m-d');
    $nonce = wp_create_nonce('addToCart');
    $sign = get_woocommerce_currency_symbol();
    $view = new View('front/frontReservationBlock');
    $view->setVariables('groups',$groups->getGroups());
    $view->setVariables('handler',Configuration::get('handler'));
    $view->setVariables('sign',$sign);
    $view->setVariables('nonce',$nonce);
    $view->setVariables('today',$today);
    if (Configuration::get('savedProduct')) {
      $savedProduct = wc_get_product(Configuration::get('savedProduct'))->get_regular_price();
      $view->setVariables('savedProduct',$savedProduct);
    }

    echo $view->render();
  }

  /**
   * Zapisanie danych z zamówienia do modułu
   */

  public function frontDoReservation($order_id)
  {
    $data = array();
    $order = new WC_Order($order_id);
    $data['first_name'] = get_post_meta($order_id,'_billing_first_name',true);
    $data['last_name'] = get_post_meta($order_id,'_billing_last_name',true);
    $data['phone'] = get_post_meta($order_id,'_billing_phone',true);
    $data['email'] = get_post_meta($order_id,'_billing_email',true);
    $data['socks'] = $_SESSION['socks'];
    $data['date'] = $_SESSION['date'];
    foreach ($order->get_items() as $item) {
      $id = $item->get_product_id();
      $product = wc_get_product($id);
      if ($product->is_type('kangu_reservation')) {
        $data['hour'] = $_SESSION['hour'];
        $data['quantity'] = $item->get_quantity();
      } else {
        if ($data['socks']) {
          $data['socks_quantity'] = $item->get_quantity();
        } else {
          $data['socks'] = false;
          $data['socks_quantity'] = 0;
        }
      }
    }
    $data['price'] = get_post_meta($order_id,'_order_total',true);
    $data['group'] = $_SESSION['group'];

    $data['commonQuantity'] = $_SESSION['commonQuantity'];

    $r = new Reservations;
    $r->setFirstName($data['first_name']);
    $r->setLastName($data['last_name']);
    $r->setPhone($data['phone']);
    $r->setEmail($data['email']);
    $r->setQuantity((int)$data['quantity']);
    $r->setSocks((bool)$data['socks']);
    $r->setSocksQuantity((int)$data['socks_quantity']);
    $r->setIdGroup((int)$data['group']);
    $r->setIdHour((int)$data['hour']);
    $r->setPrice((float)$data['price']);
    $r->setCreated(date('Y-m-d H:i:s'));
    $r->save();
    // Configuration::clearCache();

    $hour = (new Hours($data['hour']))->getHourObj()->id_product;
    $product = wc_get_product((int)$hour);
    $group = (new Group((int)$data['group']))->getGroupObj()->name;
    $vars = array(
      '{{firstname}}' => $data['first_name'],
      '{{lastname}}' => $data['last_name'],
      '{{phone}}' => $data['phone'],
      '{{email}}' => $data['email'],
      '{{quantity}}' => $data['quantity'],
      '{{socks_quantity}}' => $data['socks_quantity'],
      '{{hour}}' => $product->get_title(),
      '{{group}}' => $group,
      '{{total_price}}' => $data['price'],
      '{{date}}' => $data['date']
    );
    $content = Configuration::get('emailTemplate');

    foreach ($vars as $key => $value) {
      $content = str_replace($key, $value, $content);
    }

    if ($data['commonQuantity'] == 1) {
      $obj = new QuantitiesDates;
      $id = $obj->getBy($data['date'],$hour);

      $commonQuantity = new QuantitiesDates((int)$id['id']);
      $commonQuantity->setQuantity((int)$commonQuantity->getQuantitiesDatesObj()->quantity - (int)$data['quantity']);
      $commonQuantity->update();


    } else {
      $hour = new Hours((int)$data['hour']);
      $hour->setQuantity((int)$hour->getHourObj()->quantity - (int)$data['quantity']);
      $hour->update();
    }
    $headers = array('Content-Type: text/html; charset=UTF-8');
    $subject = __('Mail Podziękowania','kanguplugin');
    wp_mail($data['email'],$subject,$content,$headers);
    // var_dump($data);
    // var_dump($commonQuantity);
    // die();
  }
}
