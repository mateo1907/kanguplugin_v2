<?php

namespace KanguPlugin;

/**
 * Klasa obsługująca żądania JSON
 */

class JSON
{

  public static function encode($value)
  {
    $json = json_encode($value);
    return $json;
  }

  public static function decode($array)
  {
    return json_decode($json);
  }
}
