<?php

namespace KanguPlugin;

/**
 * Klasa konfiguracyjna
 */

class Configuration
{

  public static $tmpCss;

  public static $tmpJs;

  public static function set($name,$value)
  {
    update_option($name,$value);
    return true;
  }

  public static function get($name)
  {
    if (!get_option($name)) {
      return false;
    }
    return get_option($name);
  }

  public function addAdminJS($js)
  {


      if (is_array($js)) {
        foreach ($js as $one) {
          wp_enqueue_script($one,KANGUPLUGIN_DIR.'public/js/'.$one.'.js',array('jquery','wp-util'));
        }
      } else {
          wp_enqueue_script($js,KANGUPLUGIN_DIR.'public/js/'.$js.'.js',array('jquery','wp-util'));

      }
  }

  public function addFrontJS($js, $args)
  {

          wp_enqueue_script($js,KANGUPLUGIN_DIR.'public/js/'.$js.'.js',array('jquery'));
          wp_localize_script($js,$js,$args);
  }

  public function addAjaxurl()
  {
    echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
  }

  public function addAdminCSS($css)
  {
    if (is_array($css)) {
      foreach ($css as $one) {
        wp_enqueue_style($css,KANGUPLUGIN_DIR.'public/css/'.$one.'.css');
      }
    } else {
        wp_enqueue_style($css,KANGUPLUGIN_DIR.'public/css/'.$css.'.css');
    }
  }

  public static function getTableName($class)
  {
    $data = new $class;

    return $data->getFullTableName();
  }

  public static function setDefaultTimezone()
  {
    date_default_timezone_set(self::get('timezone'));
    return true;
  }

  public static function clearCache()
  {
    $array = ['price','commonQuantity','group','hour','socks'];

    foreach ($_SESSION as $key => $value) {
      if (in_array($key, $array)) {
        unset($_SESSION[$key]);
      }
    }
  }


}
