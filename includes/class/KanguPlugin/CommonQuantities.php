<?php
namespace KanguPlugin;

class CommonQuantities extends Model
{
  protected $id;
  protected $id_product;
  protected $quantity;

  protected $wpdb;
  protected $table = 'kangu_common_quantities';
  protected $fillable = ['id_product','quantity'];
  protected $commonQuantitiesObj;


  public function __construct($id=null)
  {
    global $wpdb;
    $this->wpdb = $wpdb;

    if ($id) {
      $this->commonQuantitiesObj = $this->wpdb->get_row("SELECT * FROM {$this->getFullTableName()} WHERE id = {$this->id}");
    }
  }

  public function getFullTableName()
  {
    return $this->wpdb->prefix . $this->table;
  }

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Id Product
     *
     * @return mixed
     */
    public function getIdProduct()
    {
        return $this->id_product;
    }

    /**
     * Set the value of Id Product
     *
     * @param mixed id_product
     *
     * @return self
     */
    public function setIdProduct($id_product)
    {
        $this->id_product = $id_product;

        return $this;
    }

    /**
     * Get the value of Quantity
     *
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set the value of Quantity
     *
     * @param mixed quantity
     *
     * @return self
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function check()
    {
      $q = $this->wpdb->get_var("SELECT COUNT(*) FROM {$this->getFullTableName()} WHERE id_product = {$this->getIdProduct()}");

      if ($q > 0) {
        return false;
      }
      return true;
    }

    public function getAll()
    {
      $q = $this->wpdb->get_results("SELECT * FROM {$this->getFullTableName()} WHERE 1",ARRAY_A);
      $arr = array();

      foreach ($q as $d) {
        $arr[$d['id_product']] = array(
          'id' => $d['id'],
          'quantity' => $d['quantity']
        );
      }
      return $arr;
    }

    public function getByIdProduct($id)
    {
      $q = $this->wpdb->get_row("SELECT * FROM {$this->getFullTableName()} WHERE id_product = {$id}",ARRAY_A);

      $this->id = (int)$q['id'];
      $this->id_product = (int)$q['id_product'];
      $this->quantity = (int)$q['quantity'];

      return $this;
    }

}
