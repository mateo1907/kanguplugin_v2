<?php
namespace KanguPlugin;

class Products
{
  public static function getAllKanguProducts()
  {
    global $wpdb;
    $query_args = array(
       'post_type' => 'product',
       'tax_query' => array(
            array(
                'taxonomy' => 'product_type',
                'field'    => 'slug',
                'terms'    => 'kangu_reservation',
            ),
        ),
        'posts_per_page' => -1,
     );

     $products = get_posts($query_args);

     return $products;
  }
}
