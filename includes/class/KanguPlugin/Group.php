<?php

namespace KanguPlugin;

/**
 * Grupy użytkowników według ktorych okreslane beda ceny rezerwacji
 */

class Group extends Model
{
  protected $id ;

  protected $name;

  protected $error;

  protected $max_quantity;

  protected $options;

  protected $created;

  protected $groupObj;

  protected $fillable = ['name','max_quantity','options','created'];

  public $table = 'kangu_groups';

  protected $wpdb;


      public function __construct($id = null)
      {
        global $wpdb;
        $this->wpdb = $wpdb;

        if ($id != null) {
          $this->id = $id;
          $this->groupObj = $this->wpdb->get_row("SELECT * FROM {$this->getFullTableName()} WHERE id = {$this->id}");
        }



      }



      /**
       * Pełna nazwa tabeli wraz z prefiksem
       */

      public function getFullTableName()
      {
        return $this->wpdb->prefix . $this->table;
      }
      /**
       * Get the value of Grupy użytkowników według ktorych okreslane beda ceny rezerwacji
       *
       * @return mixed
       */
      public function getName()
      {
          return $this->name;
      }

      /**
       * Set the value of Grupy użytkowników według ktorych okreslane beda ceny rezerwacji
       *
       * @param mixed name
       *
       * @return self
       */
      public function setName($name)
      {
          $this->name = $name;

          return $this;
      }

      /**
       * Get the value of Grupy użytkowników według ktorych okreslane beda ceny rezerwacji
       *
       * @return mixed
       */
      public function getError()
      {
          return $this->error;
      }

      /**
       * Set the value of Grupy użytkowników według ktorych okreslane beda ceny rezerwacji
       *
       * @param mixed name
       *
       * @return self
       */
      public function setError($error)
      {
          $this->error = $error;

          return $this;
      }

      public function getGroupObj()
      {
        return $this->groupObj;
      }

      public function getOptions()
      {
        return $this->options;
      }

      public function setOptions($options)
      {
        $this->options = $options;
        return $this;
      }

      public function getGroup($id = false)
      {

        if (!is_int($id) || !$id) {
          if (!$this->id || !is_int($this->id)) {
            return false;
          } else {
            $id = $this->id;
          }
        }
        $result = $this->wpdb->get_row("SELECT * FROM {$this->getFullTableName()} WHERE id = $id ");

        return $result;
      }

      public function getGroups()
      {

        $result = $this->wpdb->get_results("SELECT * FROM {$this->getFullTableName()} ");
        $arr = array();
        foreach ($result as $r) {
          $options = unserialize($r->options);
          $arr[] = [
            'id' => $r->id,
            'name' => $r->name,
            'max_quantity' => $r->max_quantity,
            'options' => $options,
            'created' => $r->created
          ];
        }
        return $arr;
      }


    /**
     * Get the value of Created
     *
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set the value of Created
     *
     * @param mixed created
     *
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }



    /**
     * Get the value of Max Quantity
     *
     * @return mixed
     */
    public function getMaxQuantity()
    {
        return $this->max_quantity;
    }

    /**
     * Set the value of Max Quantity
     *
     * @param mixed maxQuantity
     *
     * @return self
     */
    public function setMaxQuantity($max_quantity)
    {
        $this->max_quantity = $max_quantity;

        return $this;
    }



}
