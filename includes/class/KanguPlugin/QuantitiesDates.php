<?php
namespace KanguPlugin;

class QuantitiesDates extends Model
{
  protected $id;
  protected $id_common_quantity;
  protected $date;
  protected $quantity;
  protected $fillable = ['id_common_quantity','date','quantity'];
  protected $table = 'kangu_quantities_dates';
  protected $wpdb;

  protected $quantitiesDatesObj;

  public function __construct($id = null)
  {
    global $wpdb;
    $this->wpdb = $wpdb;

    if ($id) {
      $this->id = $id;
      $this->quantitiesDatesObj = $this->wpdb->get_row("SELECT * FROM {$this->getFullTableName()} WHERE id = {$this->id}");

    }
  }

  public function getFullTableName()
  {
    return $this->wpdb->prefix . $this->table;
  }

  public function getQuantitiesDatesObj()
  {
    return $this->quantitiesDatesObj;
  }
    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Id Common Quantity
     *
     * @return mixed
     */
    public function getIdCommonQuantity()
    {
        return $this->id_common_quantity;
    }

    /**
     * Set the value of Id Common Quantity
     *
     * @param mixed id_common_quantity
     *
     * @return self
     */
    public function setIdCommonQuantity($id_common_quantity)
    {
        $this->id_common_quantity = $id_common_quantity;

        return $this;
    }

    /**
     * Get the value of Date
     *
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of Date
     *
     * @param mixed date
     *
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    public function getQuantity()
    {
      return $this->quantity;
    }

    public function setQuantity($quantity)
    {
      $this->quantity = $quantity;
      return $this;
    }

    public function getBy($date = null, $hour=null)
    {
      $commonQuantities = Configuration::getTableName(CommonQuantities::class);
      $arr = array();
      if ($date && $hour) {

        $q = $this->wpdb->get_row("SELECT quantitiesDates.id, quantitiesDates.date, commonQuantities.id_product, quantitiesDates.quantity FROM {$this->getFullTableName()} quantitiesDates INNER JOIN {$commonQuantities} commonQuantities ON quantitiesDates.id_common_quantity = commonQuantities.id WHERE quantitiesDates.date = '{$date}' AND commonQuantities.id_product = {$hour}  ");

        $arr = array(
          'id' => $q->id,
          'date' => $q->date,
          'quantity' => $q->quantity
        );

      } elseif ($date) {
        $q = $this->wpdb->get_results("SELECT quantitiesDates.id, quantitiesDates.date, commonQuantities.id_product, commonQuantities.quantity FROM {$this->getFullTableName()} quantitiesDates INNER JOIN {$commonQuantities} commonQuantities ON quantitiesDates.id_common_quantity = commonQuantities.id WHERE quantitiesDates.date = '{$date}'");

      } elseif ($hour) {
        # code...
      }

      if (!$q) {
        return false;
      }





      return $arr;



    }

}
