<?php

namespace KanguPlugin;

/**
 * Klasa obsługująca daty
 */

class Date extends Model
{
  protected $id;

  protected $date;

  protected $status;

  protected $active;

  protected $created;

  protected $error;

  public $table = 'kangu_date';

  protected $wpdb;

  protected $dateObj;

  protected $fillable = ['date','status','active','created'];

  public function __construct($id = null)
  {
    global $wpdb;

    $this->wpdb = $wpdb;

    if ($id) {
      $this->id = $id;
      $this->dateObj = $this->wpdb->get_row("SELECT * FROM {$this->getFullTableName()} WHERE id = {$this->id}");
    }
  }

  public function getFullTableName()
  {
    return $this->wpdb->prefix . $this->table;
  }

    /**
     * Get the value of Klasa obsługująca dni świąteczne
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Klasa obsługująca dni świąteczne
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Date
     *
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of Date
     *
     * @param mixed date
     *
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    public function getDateObj()
    {
      return $this->dateObj;
    }
    /**
     * Get the value of Holiday
     *
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of Holiday
     *
     * @param mixed holiday
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of Holiday
     *
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set the value of Holiday
     *
     * @param mixed holiday
     *
     * @return self
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }






    /**
     * Get the value of Created
     *
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set the value of Created
     *
     * @param mixed created
     *
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    public function getError()
    {
      return $this->error;
    }

    public function setError($error)
    {
      $this->error = $error;
      return $this;
    }

    public function checkExist($date)
    {
      $q = $this->wpdb->get_var( "SELECT COUNT(*) FROM {$this->getFullTableName()} WHERE date = '{$date}'" );

      if ((int)$q > 0) {
        $this->setError("{$date} już istnieje w bazie");
        return false;
      }
      return true;
    }

    public function getAllDates()
    {

      $dates = $this->wpdb->get_results("SELECT * FROM {$this->getFullTableName()} WHERE 1 ");

      return $dates;
    }

    /**
     * Metoda startująca wraz z pluginem - wyłącza "stare" daty
     */

    public function disableOldDates()
    {
      $currentDate = date('Y-m-d');

      $this->wpdb->query("UPDATE {$this->getFullTableName()} SET active = 0 WHERE date < '{$currentDate}'");
      return true;
    }



}
