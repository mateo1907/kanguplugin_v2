<?php
namespace KanguPlugin;

class Prices extends Model
{
  protected $id;

  protected $id_group;

  protected $price;

  protected $status;

  protected $created;

  protected $error;

  protected $wpdb;

  protected $priceObj;

  protected $fillable = ['id_group','price','status','created'];

  protected $table = 'kangu_prices';

  public function __construct($id = null)
  {
    global $wpdb;
    $this->wpdb = $wpdb;

    if ($id) {
      $this->id = $id;
      $this->priceObj = $this->wpdb->get_row("SELECT * FROM {$this->getFullTableName()} WHERE id = {$this->id}");
    }
  }

  public function getFullTableName()
  {
    return $this->wpdb->prefix . $this->table;
  }

    /**
     * Get the value of Id Group
     *
     * @return mixed
     */
    public function getIdGroup()
    {
        return $this->id_group;
    }


    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }


    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set the value of Id Group
     *
     * @param mixed id_group
     *
     * @return self
     */
    public function setIdGroup($id_group)
    {
        $this->id_group = $id_group;

        return $this;
    }

    /**
     * Get the value of Price
     *
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set the value of Price
     *
     * @param mixed price
     *
     * @return self
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }


    public function getPriceObj()
    {
      return $this->priceObj;
    }
    /**
     * Get the value of Status
     *
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of Status
     *
     * @param mixed status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of Error
     *
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set the value of Error
     *
     * @param mixed error
     *
     * @return self
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }



    /**
     * Pobranie wszystkich cen
     */
    public function getPrices()
    {
      $groupTable = Configuration::getTableName(Group::class);
      $prices = $this->wpdb->get_results("SELECT prices.id, groups.name, prices.status, prices.price, prices.created FROM {$this->getFullTableName()} prices INNER JOIN {$groupTable} groups ON prices.id_group = groups.id  WHERE 1");

      return $prices;
    }

    /**
     * Sprawdzenie, czy istnieje cena dla podanej grupy użytkowników
     */

    public function checkExist($group_id, $status)
    {
      $q = $this->wpdb->get_var("SELECT COUNT(*) FROM {$this->getFullTableName()} WHERE id_group = {$group_id} AND status= {$status}");

      if ((int)$q > 0 ) {
        $this->setError("Cena dla tej grupy już istnieje!");
        return false;
      }
      return true;
    }

    public function getPriceBy($group = null, $status = null)
    {
      if ($group && $status) {
        $q = $this->wpdb->get_var("SELECT price FROM {$this->getFullTableName()} WHERE id_group = {$group} AND status = {$status}");
      } else
      if ($group && !$status) {
        $q = $this->wpdb->get_var("SELECT price FROM {$this->getFullTableName()} WHERE id_group = {$group} ");
      } else
      if ($status && !$group) {
        $q = $this->wpdb->get_var("SELECT price FROM {$this->getFullTableName()} WHERE status = {$status}");
      }

      return $q;
    }

}
