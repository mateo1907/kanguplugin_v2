<?php
namespace KanguPlugin;
/**
 * Klasa bazowa dla pluginu - admin
 */


class Admin
{

  /**
   * Inicjalizacja panelu admina
   * @return [type] [description]
   */
  public function init()
  {

    add_action('admin_menu',array($this,'createAdminMenu'));
    $conf = new Configuration;
    $conf->addAdminJs('admin');
    $conf->addAdminCss('admin');


    $date = new Date;
    $date->disableOldDates();


  }

  /**
   * Tworzenie menu i submenu
   * @return [type] [description]
   */
  public function createAdminMenu()
  {
    add_menu_page('KanguJump Plugin v2','KanguJump Plugin v2','manage_options','kangujumpplugin',array($this,'adminPluginBlockBody'));
    add_submenu_page('kangujumpplugin','Rezerwacje','Rezerwacje','manage_options','kangujumppluginreservations',array($this,'adminReservationsBlockBody'));
    add_submenu_page('kangujumpplugin','Grupy','Grupy','manage_options','kangujumpplugingroups',array($this,'adminGroupsBlockBody'));
    add_submenu_page('kangujumpplugin','Daty','Daty','manage_options','kangujumpplugindate',array($this,'adminDateBlockBody'));
    add_submenu_page('kangujumpplugin','Ceny','Ceny','manage_options','kangujumppluginprices',array($this,'adminPricesBlockBody'));
    add_submenu_page('kangujumpplugin','Wspólne ilości miejsc','Wspólne ilości miejsc','manage_options','kangujumpplugincommonquantities',array($this,'adminCommonQuantitiesBlockBody'));
    add_submenu_page('kangujumpplugin','Szablon E-mail','Szablon E-mail','manage_options','kangujumppluginemailtemplate',array($this,'adminEmailTemplateBlockBody'));
    add_submenu_page('kangujumpplugin','Opcje','Opcje','manage_options','kangujumppluginoptions',array($this,'adminOptionsBlockBody'));
  }

  /**
   * Metoda wyswietlajaca widok Pluginu
   * @return [type] [description]
   */
  public function adminPluginBlockBody()
  {
    $view = new View('admin/adminBlock');
    $view->setVariables('test','Something');
    echo $view->render();
  }

  /**
   * Widok - opcje pluginu
   * @return [type] [description]
   */
  public function adminOptionsBlockBody()
  {
    $query_args = array(
       'post_type' => 'product',
       'posts_per_page' => -1,
       'tax_query' => array(
            array(
                'taxonomy' => 'product_type',
                'field'    => 'slug',
                'terms'    => 'simple',
            ),
        ),
     );
    $products = get_posts($query_args);

    $args = array(
    	'sort_order' => 'asc',
    	'sort_column' => 'post_title',
    	'post_type' => 'page',
    	'post_status' => 'publish',
      'posts_per_page' => -1
    );
    $pages = get_pages($args);
    $conf = new Configuration;
    $conf->addAdminJS('admin-options');
    $view = new View('admin/adminOptionsBlock');
    $view->setVariables('handler',Configuration::get('handler'));
    $view->setVariables('title','Opcje');
    $view->setVariables('products',$products);
    $view->setVariables('pages',$pages);
    if (Configuration::get('savedProduct')) {
      $savedProduct = wc_get_product(Configuration::get('savedProduct'));
      $view->setVariables('savedProduct',$savedProduct->get_title());
    }

    echo $view->render();
  }

  /**
   * Widok - grupy użytkowników
   */
  public function adminGroupsBlockBody()
  {


    $groups = new Group;
    $nonce = wp_create_nonce('actionAddGroup');

    $conf = new Configuration;
    $conf->addAdminJs(['admin-groups']);

    $json = JSON::encode(['data'=>'test']);
    $view = new View('admin/adminGroupsBlock');
    $view->setVariables('groups',$groups->getGroups());
    $view->setVariables('title','Grupy');
    $view->setVariables('nonce', $nonce);
    $view->setVariables('json',$json);
    echo $view->render();
  }

  /**
   * Widok - daty
   */

  public function adminDateBlockBody()
  {
    $products = Products::getAllKanguProducts();
    $savedQuantities = new CommonQuantities;
    // $dates = new Date();
    // $allDates = $dates->getAllDatesByStatus(0);
    // $groups = new Group();
    // $allGroups = $groups->getGroups();
    $conf = new Configuration;
    $conf->addAdminJs(['timepicker','admin-date']);
    $nonce = wp_create_nonce('actionAddDate');
    $deleteNonce = wp_create_nonce('deleteNonce');
    $addHourNonce = wp_create_nonce('actionAddHour');
    $view = new View('admin/adminDateBlockBody');
    $view->setVariables('title','Daty');
    $view->setVariables('nonce',$nonce);
    $view->setVariables('products',$products);
    $view->setVariables('savedQuantities',$savedQuantities->getAll());
    $view->setVariables('dates',$allDates);
    $view->setVariables('groups',$allGroups);
    $view->setVariables('deleteNonce',$deleteNonce);
    $view->setVariables('addHourNonce',$addHourNonce);
    echo $view->render();
  }

  /**
   * Widok - rezerwacje
   */
  public function adminReservationsBlockBody()
  {
    $conf = new Configuration;
    $conf->addAdminJs('admin-reservations');
    $groups = new Group();
    $nonce = wp_create_nonce('addReservation');
    $deleteNonce = wp_create_nonce('deleteReservation');
    $view = new View('admin/adminReservationsBlockBody');
    $view->setVariables('title','Rezerwacje');
    $view->setVariables('groups',$groups->getGroups());
    $view->setVariables('nonce',$nonce);
    $view->setVariables('deleteNonce',$deleteNonce);

    echo $view->render();
  }

  /**
   * Widok ceny
   */
  public function adminPricesBlockBody()
  {
    $conf = new Configuration;
    $conf->addAdminJs('admin-prices');
    $groups = new Group();
    $nonce = wp_create_nonce('actionAddPrice');
    $deleteNonce = wp_create_nonce('actionDeletePrice');
    $view = new View('admin/adminPricesBlockBody');
    $view->setVariables('title','Ceny');
    $view->setVariables('groups',$groups->getGroups());
    $view->setVariables('nonce',$nonce);
    $view->setVariables('deleteNonce',$deleteNonce);
    $view->setVariables('status',$status);
    echo $view->render();
  }

  public function adminCommonQuantitiesBlockBody()
  {
    $conf = new Configuration;
    $conf->addAdminJs('admin-commonQuantities');
    $products = Products::getAllKanguProducts();
    $savedQuantities = new CommonQuantities;
    $nonce = wp_create_nonce('addCommonQuantities');
    $view = new View('admin/adminCommonQuantitiesBlockBody');
    $view->setVariables('title','Wspólne ilości miejsc');
    $view->setVariables('products',$products);
    $view->setVariables('savedQuantities',$savedQuantities->getAll());
    $view->setVariables('nonce',$nonce);
    echo $view->render();

  }

  public function adminEmailTemplateBlockBody()
  {
    if ($_POST['content']) {
      Configuration::set('emailTemplate',stripslashes(wp_filter_post_kses(addslashes($_POST['content']))));
    }
    $content = Configuration::get('emailTemplate');
    $view = new View('admin/adminEmailBlockBody');
    $view->setVariables('title','Szablon E-mail');
    $view->setVariables('content',$content);
    echo $view->render();


  }



}
