<?php
namespace KanguPlugin;


class Db
{

  public $groupsTable;
  public $dateTable;
  public $hoursTable;
  public $pricesTable;
  public $reservationsTable;
  public $holidaysTable;
  public $commonQuantitiesTable;
  public $quantitiesDatesTable;
  protected $wpdb;
  protected $charset_collate;

  public function __construct()
  {
    global $wpdb;
    $this->wpdb = $wpdb;
    $this->charset_collate = $wpdb->get_charset_collate();

    $this->groupsTable = Configuration::getTableName(Group::class);
    $this->dateTable =  Configuration::getTableName(Date::class);
    $this->hoursTable = Configuration::getTableName(Hours::class);
    $this->pricesTable = Configuration::getTableName(Prices::class);
    $this->reservationsTable = Configuration::getTableName(Reservations::class);
    $this->holidaysTable = Configuration::getTableName(Holidays::class);
    $this->commonQuantitiesTable = Configuration::getTableName(CommonQuantities::class);
    $this->quantitiesDatesTable = Configuration::getTableName(QuantitiesDates::class);
    register_activation_hook(__FILE__,array($this,'createDatabases'));
  }
  /**
   * Utworzenie tabel w bazie danych
   * @return [type] [description]
   */
  public function createDatabases()
  {
    $sql[1] = "CREATE TABLE IF NOT EXISTS $this->groupsTable (
        id int(11) NOT NULL AUTO_INCREMENT,
        name varchar(255) NOT NULL,
        max_quantity int(11) NOT NULL,
        options text NOT NULL,
        created datetime NOT NULL,
        PRIMARY KEY (id)
      ) $this->charset_collate; ";

    $sql[2] = "CREATE TABLE IF NOT EXISTS $this->dateTable (
          id int(11) NOT NULL AUTO_INCREMENT,
          date date NOT NULL,
          -- status int(1) NOT NULL,
          active boolean NOT NULL,
          created datetime NOT NULL,
          PRIMARY KEY (id)
        ) $this->charset_collate; ";
    $sql[3] = "CREATE TABLE IF NOT EXISTS $this->hoursTable (
              id int(11) NOT NULL AUTO_INCREMENT,
              id_product int(11) NOT NULL,
              id_group int(11) NOT NULL,
              id_date int(11) NOT NULL,

              quantity int(11) NOT NULL,
              -- commonQuantity int(11) NOT NULL,
              created datetime NOT NULL,
              PRIMARY KEY (id)
            ) $this->charset_collate; ";

      $sql[4] = "CREATE TABLE IF NOT EXISTS $this->pricesTable (
          id int(11) NOT NULL AUTO_INCREMENT,
          id_group int(11) NOT NULL,
          price float NOT NULL,
          status int(1) NOT NULL,
          created datetime NOT NULL,
          PRIMARY KEY (id)
        ) $this->charset_collate; ";

        $sql[5] = "CREATE TABLE IF NOT EXISTS $this->reservationsTable (
            id int(11) NOT NULL AUTO_INCREMENT,
            id_group int(11) NOT NULL,
            id_hour int(11) NOT NULL,
            firstname varchar(255) NOT NULL,
            lastname varchar(255) NOT NULL,
            phone varchar(255) NOT NULL,
            email varchar(255) NOT NULL,
            quantity int(11) NOT NULL,
            price float NOT NULL,
            socks boolean NOT NULL,
            socks_quantity int(11) NOT NULL,
            created datetime NOT NULL,
            PRIMARY KEY (id)
          ) $this->charset_collate; ";

          $sql[6] = "CREATE TABLE IF NOT EXISTS $this->holidaysTable (
              id int(11) NOT NULL AUTO_INCREMENT,
              date varchar(255) NOT NULL,
              PRIMARY KEY (id)
            ) $this->charset_collate; ";

            $sql[7] = "CREATE TABLE IF NOT EXISTS $this->commonQuantitiesTable (
                id int(11) NOT NULL AUTO_INCREMENT,
                id_product int(11) NOT NULL,
                quantity int(11) NOT NULL,
                PRIMARY KEY (id)
              ) $this->charset_collate; ";

          $sql[8] = "CREATE TABLE IF NOT EXISTS $this->quantitiesDatesTable (
              id int(11) NOT NULL AUTO_INCREMENT,
              id_common_quantity int(11) NOT NULL,
              date date NOT NULL,
              quantity int(11) NOT NULL,
              PRIMARY KEY (id)
            ) $this->charset_collate; ";

    require_once(ABSPATH. 'wp-admin/includes/upgrade.php');

    foreach ($sql as $s) {
      dbDelta($s);
    }
  }


}
