<?php
function kanguplugin_load_textdomain() {
  load_plugin_textdomain( 'kanguplugin', false, basename( KANGUPLUGIN_DIR ) . '/lang' );
}

/**
 * Tablica statusów
 */

$status[0] = 'Poniedziałek - Czwartek';
$status[1] = 'Piątek - Niedziela / Święta';


/**
 *  Rejestracja typu produktu
 */

function register_product_type()
{
  class WC_Product_Kangu_Reservation extends WC_Product
  {

    function __construct($product)
    {
      $this->product_type = 'kangu_reservation';
      parent::__construct($product);
    }
  }
}
add_action('init','register_product_type');

function setTypeName($types)
{
  $types['kangu_reservation'] = __('Kangu Product Type');

  return $types;
}
add_filter('product_type_selector','setTypeName');

wp_enqueue_style('confirm','https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css');
wp_enqueue_script('confirmjs','https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js',array('jquery'));
wp_enqueue_script('moment','//cdn.jsdelivr.net/momentjs/latest/moment.min.js');
wp_enqueue_script('moment-timezone','https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.14/moment-timezone.min.js');
wp_enqueue_script('moment-timezone-data','https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.14/moment-timezone-with-data.min.js');
wp_enqueue_script('range','https://cdnjs.cloudflare.com/ajax/libs/moment-range/3.1.0/moment-range.min.js');
wp_register_script('datetimepicker','https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.js');
 wp_enqueue_script('datetimepicker');
 wp_register_style('datetimepicker','https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.min.css');
  wp_enqueue_style('datetimepicker');
  wp_register_style('fa','https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
   wp_enqueue_style('fa');
// wp_enqueue_script( array('jquery','wp-util') );

// Ustawienie ceny w zaleznosci od grupy i statusu
add_action( 'woocommerce_before_calculate_totals', 'setPriceByGroupAndStatus' );
function setPriceByGroupAndStatus($cart_object)
{
  foreach ( $cart_object->get_cart() as $hash => $value ) {
    if ($value['data']->is_type('kangu_reservation')) {
      $newprice = $_SESSION['price'];
      $value['data']->set_price( $newprice );
    }



}
}
