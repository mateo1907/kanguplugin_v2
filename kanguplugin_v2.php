<?php

/**
* Plugin Name: KanguJump Plugin v2
* Description: Poprawiona wersja pluginu rezerwacji KanguJump
* Version: 2.0
* Author: Mateusz Prasał
* License: GPL
* Text Domain: kanguplugin
* Domain Path: /lang
*/

define('KANGUPLUGIN_DIR', plugin_dir_url( __FILE__ ));
include 'includes/functions/functions.php';
require 'vendor/autoload.php';
add_action( 'plugins_loaded', 'kanguplugin_load_textdomain' );




use \KanguPlugin\Admin;
use \KanguPlugin\Db;
use \KanguPlugin\Configuration;
use \KanguPlugin\Front;
use \KanguPlugin\Ajax;
$db = new Db;
$db->createDatabases();


$ajax = new Ajax();
$ajax->initAdmin();
$ajax->initFront();
if (is_admin()) {
  $admin = new Admin;
  $admin->init();
} else {
  $front = new Front;
  $front->init();
}
