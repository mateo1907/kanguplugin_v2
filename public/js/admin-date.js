function getAllKanguProducts()
{
  jQuery(document).ready(function($){

    var data = {
      'action' : 'adminActionShowKanguProducts'
    }
    $.ajax({
      type:'get',
      url:ajaxurl,
      data:data
    })
    .success(function(response){
      var json = $.parseJSON(response)
      // console.log(json)
      var productsTemplate = _.template($('#tmpl-productsTemplate').html());
      $('#hours').html(productsTemplate(json));
    })
  })
}

function getAllDates()
{

  jQuery(document).ready(function($){
    $('.loader').show()
    var data = {
      'action' : 'adminActionGetAllDates',
      // 'status' : $('#statusSelect').val(),

    }

    $.ajax({
      type:'get',
      url:ajaxurl,
      data:data
    })
    .success(function(response){
      var json = $.parseJSON(response)
      var datesTemplate = _.template($('#tmpl-datesTemplate').html());
      $('#dates').html(datesTemplate(json));
      $('.loader').hide()
    })
  })
}

function getAllHolidays()
{
  jQuery(document).ready(function($){
    $('.loader').show()
    var data = {
      action : 'adminActionGetAllHolidays'
    }
    $.ajax({
      method:'get',
      url:ajaxurl,
      data:data
    })
    .success(function(response){
      var json = $.parseJSON(response)
      var holidaysTemplate = _.template($('#tmpl-holidaysTemplate').html());
      $('#holidaysDiv').html(holidaysTemplate(json));
      $('.loader').hide()

    })
  })
}

function getAllGroups()
{
  jQuery(document).ready(function($){
    var data = {
      'action' : 'adminActionShowGroups'
    }
    $.ajax({
      type:'get',
      url:ajaxurl,
      data:data
    })
    .success(function(response){
      var json = $.parseJSON(response)
      var groupsTemplate = _.template($('#tmpl-groupsTemplate').html())
      $('#groups').html(groupsTemplate(json));
    })
  })
}
jQuery(document).ready(function($){
  $('.dateHead').toggle(function(){
    $('#content_'+$(this).attr('data-id')).slideDown();
  }, function(){
    $('#content_'+$(this).attr('data-id')).slideUp();
  })
  // getAllKanguProducts()
  getAllDates()
  getAllGroups()
  getAllHolidays()
  jQuery.datetimepicker.setLocale('pl');
  jQuery('#date').datetimepicker({
    'timepicker':false,
    'format': 'Y-m-d'
  });

  jQuery('#from').datetimepicker({
    'timepicker':false,
    'format': 'Y-m-d'
  });

  jQuery('#to').datetimepicker({
    'timepicker':false,
    'format': 'Y-m-d'
  });

  jQuery('#holiday').datetimepicker({
    'timepicker':false,
    'format': 'Y-m-d'
  });


$('input[name=type]').on('click',function(){
  if ($(this).val() == 1) {
    $('.status').show()
    $('.type_2').hide()
    $('.type_1').show()
  } else {
    $('.status').show()
    $('.type_1').hide()
    $('.type_2').show()
  }
})

$('#actionAddDateForm').on('submit',function(){
  var checked = false;
  $('input[name=type]').each(function(){
    if ($(this).is(':checked')) {
      checked = $(this).val();
    }
  })

  if (checked == false ) {
    $.alert({
    title: 'Błąd!',
    content: 'Wybierz typ wprowadzania daty',
  });
  }

  switch (checked) {
    case "1":
      var date = $('#date').val()
      var status;
      if ($('input[name=status]').is(':checked')) {
        status = 1
      } else {
        status = 0
      }

      var data = {
        'action' : 'adminActionAddDate',
        'nonce' : $('#nonce').val(),
        'date' : date,
        'status' : status
      }

      $.ajax({
        type:'post',
        url:ajaxurl,
        data:data
      })
      .success(function(response){
        var json = $.parseJSON(response);
        var content = flashMessageTemplate(json);
        console.log(response)
        $('#messages').html(content)
      })

    break;

    case "2":
        var status;
        if ($('input[name=status]').is(':checked')) {
          status = 1
        } else {
          status = 0
        }
      var from = moment($('#from').val(),'YYYY-MM-DD')
      var to = moment($('#to').val(),'YYYY-MM-DD')
      var diff = to.diff(from,'days');
      var dateArray = [];
      window['moment-range'].extendMoment(moment);
      var range = moment.range(from,to)

      var arr = Array.from(range.by('days'))
      arr.map(function(m){
        dateArray.push(m.format('YYYY-MM-DD'))
      })

      var data = {
        'action' : 'adminActionAddDate',
        'nonce' : $('#nonce').val(),
        'date' : dateArray,
        'status' : status
      }

      $.ajax({
        type:'post',
        url:ajaxurl,
        data:data
      })
      .success(function(response){
        var json = $.parseJSON(response);
        var content = flashMessageTemplate(json);
        //
        $('#messages').html(content)
      })
    break;


  }

  return false;
})
$('#statusSelect').on('change',function(){
  $('#dates').empty();
  getAllDates()
})

$('#setToGroup').on('click',function(){
  $('#messages').empty();
  var error = false;
  var productsArray = []
  var datesArray = [];
  var group
  $('.productsCheck').each(function(){


    if ($(this).is(':checked')) {
      var v = $(this).val()
      var q = $('#productsQuantity'+v).val()
      var c = $('#commonQuantity'+v).val()

      if (q == '') {
        $.alert({
          title:'Błąd',
          content:'Wprowadź poprawne wartości'
        })
        error = true;
      } else {
        error = false;
        productsArray.push({
          product : v,
          quantity : q,
          commonQuantity : c
        })
      }

    }
  })

  $('.datesCheck').each(function(){
    if ($(this).is(':checked')) {
      datesArray.push($(this).val())
    }

  })
  if (datesArray.length == 0) {
    $.alert({
      title:"Błąd",
      content: "Zaznacz odpowiednie daty"
    })
    error = true;
  }

  if (typeof $('input[name=group]:checked').val() == 'undefined') {
    $.alert({
      title:"Błąd",
      content: "Wybierz grupę"
    })
    error = true;
  } else {
    group = $('input[name=group]:checked').val()
  }

  if (error === false) {

    var data = {
      action:'adminActionSaveHour',
      nonce: $('#addHourNonce').val(),
      products : productsArray,
      dates: datesArray,
      group : group
    }
    // console.log(data)

    $.ajax({
      type:'post',
      url:ajaxurl,
      data:data
    })
    .success(function(response){
      var json = $.parseJSON(response);


        var content = flashMessageMultipleTemplate(json);



      $('#messages').html(content)
    })
  }
})

  $(document).on('click','.group',function(){
    var max_quantity = $(this).attr('data-max_quantity');

    if (max_quantity > 0) {
      $('.productsQuantity').prop('readonly',true).val(max_quantity)
    } else {
      $('.productsQuantity').prop('readonly',false).val(0)
    }
  })

  $('#setHoliday').on('click',function(){
    var data = {
      action : 'adminActionAddHoliday',
      holiday: $('#holiday').val()
    }

    $.ajax({
      method:'post',
      url:ajaxurl,
      data:data
    })
    .success(function(response){
      console.log(response)
    })
  })

  $(document).on('click','.deleteDateIco',function(){
    var id = $(this).attr('data-id')
    var nonce = $('#deleteNonce').val();
    $.confirm({
      title: 'Potwierdź usunięcie',
      content: 'Czy na pewno chcesz usunąć ten wpis?',
      buttons: {
          Tak: function () {
            var data = {
              'action' : 'adminActionDeleteDate',
              'id' : id,
              'nonce' : nonce
            }
              $.ajax({
                type:'post',
                url:ajaxurl,
                data:data
              })
              .success(function(response){
                var json = $.parseJSON(response);
                var content = flashMessageTemplate(json);
                //
                $('#messages').html(content)
                getAllDates()

              })
          },
          Nie: function () {

          },

      }
  });
  })

  $('#refreshDates').on('click',function(e){
    e.preventDefault()
    getAllDates();
  })

  $('#getHolidays').toggle(function(){
    $('#holidaysDiv').slideDown()
  }, function(){
    $('#holidaysDiv').slideUp()
  })

  $(document).on('click','.deleteHolidayIco',function(){
    var id = $(this).attr('data-id')
    var nonce = $('#deleteNonce').val();
    $.confirm({
      title: 'Potwierdź usunięcie',
      content: 'Czy na pewno chcesz usunąć ten wpis?',
      buttons: {
          Tak: function () {
            var data = {
              'action' : 'adminActionDeleteHoliday',
              'id' : id,
              'nonce' : nonce
            }
              $.ajax({
                type:'post',
                url:ajaxurl,
                data:data
              })
              .success(function(response){
                var json = $.parseJSON(response);
                var content = flashMessageTemplate(json);
                //
                $('#messages').html(content)
                getAllHolidays()

              })
          },
          Nie: function () {

          },

      }
  });
  })

  $(document).on('click','.checkInputs',function(e){
    e.preventDefault();
    var inputs = $(this).attr('data-inputs');

    $('.'+inputs).prop('checked',true)
  })
})
