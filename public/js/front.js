
function getDatesByGroup()
{
  // jQuery(document).ready(function($){
    $('.usersGroup').on('click',function(){
      $('#changeGroup').fadeIn()
      var group = $(this).attr('data-id')
      var groupCommonQuantity = $(this).attr('data-commonquantity')
      localStorage.setItem('group',group);
      localStorage.setItem('groupCommonQuantity',groupCommonQuantity)
      $(this).addClass('notHidden groupSelected')

      $('.usersGroup').each(function(){
        if ($(this).hasClass('notHidden') == false) {
          $(this).fadeOut()
        }
      })


      localStorage.setItem('option',$('.notHidden').attr('data-option'));
      $('#datesDiv').fadeIn('slow')

      return true;
    })

  // })
}

function showQuantities(hourQuantity)
{
  if ($('.notHidden').attr('data-option') == 1) {
    $('#quantity').prop('readonly',true).val(hourQuantity)
    $('#quantity').attr('data-max_quantity',hourQuantity)
    $('#socks_quantity').attr('data-max_quantity',hourQuantity)
  } else if ($('.notHidden').attr('data-option') == 2) {
    $('#quantity').val(hourQuantity).attr('data-minQuantity',hourQuantity).attr('data-maxQuantity','')
  } else {
    $('#quantity').attr('data-max_quantity',hourQuantity)
    $('#socks_quantity').attr('data-max_quantity',hourQuantity)
  }

  $('#quantities').fadeIn()
}

function checkPrice()
{
  var price = parseFloat(localStorage.price);
  var savedProductPrice = parseFloat($('#savedProductPrice').val())
  var quantity = parseFloat($('#quantity').val())
  var socks_quantity = parseFloat($('#socks_quantity').val())
  var totalPrice;
  if ( ($('#quantity').val() !== '' && $('#socks_quantity').val() !== '') || ($('#quantity').val() !== '' && $('#no_socks').is(':checked') ) ) {
    if ($('#no_socks').is(':checked')) {
      totalPrice = price * quantity
    } else {
      totalPrice = (price * quantity) + (savedProductPrice * socks_quantity)
    }

    $('#totalPrice').text(totalPrice)
    $('#pricesDiv').fadeIn()
    $('#submitDiv').fadeIn()
  } else {
      $('#pricesDiv').fadeOut()
      $('#submitDiv').fadeOut()
  }
}
jQuery(document).ready(function($){
  $(document).on('click',function(e){
    if (e.target.className == 'kanguplugin_reservation_popup') {
      $('#app').fadeOut()
    }
  })
  jQuery.datetimepicker.setLocale('es');
  jQuery('#date').datetimepicker({
    'timepicker':false,
    'format': 'Y-m-d'
  });
  var date,
      hour,
      group;

  getDatesByGroup()

  $('#date').on('change',function(){
    $('#check').fadeIn();

  })
  $('#check').on('click',function(){
    $('#quantities').hide()
    $('.quantities').val('')
    $('#submitDiv').hide()
      $('#pricesDiv').hide()
    group = localStorage.group;
    date = $('#date').val()
    localStorage.setItem('date',date);
    $('#hours').empty()
    $('.loader').show()
    $('#hoursDiv').fadeIn()

    var data = {
      action : 'adminActionGetHoursByDateAndGroup',
      group:group,
      date:date
    }
    $.ajax({
      method:'get',
      url:ajaxurl,
      data:data
    })
    .success(function(response){
      $('#check').fadeOut()
      var json = $.parseJSON(response)
      $('.loader').hide()
      if (json.type == 'danger') {
        $('#hours').html('<div class="alert alert-danger">'+json.message+'</div>');
      } else {
        json.data.forEach(function(hour){
          $('#hours').append('<div data-status="'+hour.dateStatus+'" data-id="'+hour.id+'" data-quantity="'+hour.quantity+'" class="hour"><span>'+hour.title+'</span></div>');
        })
      }
    })
  })

  $(document).on('click','.hour',function(){
    var thisHour = $(this)
    localStorage.setItem('status',$(this).attr('data-status'))
    $('#quantities').hide()
    $('.quantities').val('')
    $('#submitDiv').hide()
    $('#pricesDiv').hide()
    $('.loader').show()
    $('.hour').each(function(){
      if ($(this).hasClass('hour_active') == true) {
        $(this).removeClass('hour_active')
      }
    })
    $(this).addClass('hour_active')

    if ($(this).hasClass('hour_active')) {
      var quantity = $(this).attr('data-quantity')
      // var commonQuantity = $(this).attr('data-commonquantity')
      var data = {
        action : 'frontActionGetPriceByGroupAndStatusAndGetCommonQuantity',
        group: localStorage.group,
        status: $(this).attr('data-status'),
        date: $('#date').val(),
        hour_id : $(this).attr('data-id')
      }

      $.ajax({
        method:'get',
        url:ajaxurl,
        data:data
      })
      .success(function(response){
        // $.alert({
        //   content:response
        // })
        var json = $.parseJSON(response)
        if (json.type == 'success') {
          localStorage.setItem('price',json.data.prices);
          console.log('price',json.data)
          var commonQuantity = json.data.quantities.quantity
          thisHour.attr('data-commonquantity',commonQuantity)
        }
        $('.loader').hide()

        if (localStorage.groupCommonQuantity == 1) {
          if ($('.notHidden').attr('data-option') == 1) {
            $('#quantity').prop('readonly',true).val(quantity)
            $('#quantity').attr('data-max_quantity',quantity)
            $('#socks_quantity').attr('data-max_quantity',quantity)
          } else if ($('.notHidden').attr('data-option') == 2) {
            $('#quantity').val(quantity).attr('data-minQuantity',quantity).attr('data-max_quantity',commonQuantity)
            $('#socks_quantity').attr('data-max_quantity',commonQuantity)
          } else {
            $('#quantity').attr('data-max_quantity',commonQuantity)
            $('#socks_quantity').attr('data-max_quantity',commonQuantity)
          }


        } else {
          if ($('.notHidden').attr('data-option') == 1) {
            $('#quantity').prop('readonly',true).val(quantity)
            $('#quantity').attr('data-max_quantity',quantity)
            $('#socks_quantity').attr('data-max_quantity',quantity)
          } else if ($('.notHidden').attr('data-option') == 2) {

            $('#quantity').val(quantity).attr('data-minQuantity',quantity).attr('data-maxQuantity','')
          } else {
            $('#quantity').attr('data-max_quantity',quantity)
            $('#socks_quantity').attr('data-max_quantity',quantity)
          }

        }
        $('#quantities').fadeIn()

      })
    }
  })

  $('.quantities').bind('change keyup mouseup',function(){
	  // console.log($(this).val())

    if (localStorage.groupCommonQuantity == 1) {
      if ($(this).attr('data-minQuantity')) {
        if ( (parseFloat($(this).val()) < parseFloat($(this).attr('data-minQuantity'))) ) {
          $.alert({
            title: 'Błąd',
            content: 'Wartosc nie moze byc mniejsza niz: '+$(this).attr('data-minQuantity')
          })
            $(this).val($(this).attr('data-minQuantity'))
        }
        if (parseFloat($(this).val()) > parseFloat($(this).attr('data-max_quantity'))) {
          $.alert({
            title: 'Błąd',
            content: front.quantity+$(this).attr('data-max_quantity')
          })
            $(this).val($(this).attr('data-minQuantity'))
        }


      }  else {
        if ( ( parseFloat($(this).val()) > parseFloat($(this).attr('data-max_quantity')) ) || parseFloat($(this).val()) < 0  ) {
          $.alert({
            title: 'Błąd',
            content: front.quantity+$(this).attr('data-max_quantity')
          })
          $(this).val($(this).attr('data-max_quantity'))
          return false
        }
      }
    } else {
      if ($(this).attr('data-minQuantity')) {
        if ( (parseFloat($(this).val()) < parseFloat($(this).attr('data-minQuantity'))) ) {
          $.alert({
            title: 'Błąd',
            content: 'Wartosc nie moze byc mniejsza niz: '+$(this).attr('data-minQuantity')
          })
            $(this).val($(this).attr('data-minQuantity'))
        }
      } else {
        if ( ( parseFloat($(this).val()) > parseFloat($(this).attr('data-max_quantity')) ) || parseFloat($(this).val()) < 0  ) {
          $.alert({
            title: 'Błąd',
            content: front.quantity+$(this).attr('data-max_quantity')
          })
          $(this).val($(this).attr('data-max_quantity'))
          return false
        }
      }

    }



    checkPrice()
  })

  $('#no_socks').on('click',function(){
    checkPrice()
  })

  $('#submit').on('click',function(){
    var socks;
    var socks_quantity

    if ($('#no_socks').is(':checked')) {
      socks = false;
      socks_quantity = 0
    } else {
      socks = true;
      socks_quantity = $('#socks_quantity').val()
    }
    var data = {
      action:'frontActionAddToCartAndChangePrice',
      nonce: $('#nonce').val(),
      group: $('.groupSelected').attr('data-id'),
      status: localStorage.status,
      date : $('#date').val(),
      hour : $('.hour_active').attr('data-id'),
      quantity : $('#quantity').val(),
      socks : socks,
      socks_quantity : socks_quantity,
      commonQuantity : localStorage.groupCommonQuantity,
      option: localStorage.option

    }
    // console.log(data)
    $.ajax({
      method:'post',
      url:ajaxurl,
      data:data
    })
    .success(function(response){
      // $.alert({
      //   content:response
      // })
      var json = $.parseJSON(response)
      if (json.msg.length > 0) {
        window.location.href = json.msg
      } else {
        console.log('blad')
      }
    })

  })

  $('#close').on('click',function(){
    $('#app').fadeOut()
  })

  $('#changeGroup').on('click',function(){
    $(this).hide()
    $('.notHidden').removeClass('notHidden groupSelected')
    $('.kanguplugin_hide').hide()
    $('#check').show()
    $('.usersGroup').fadeIn()
  })
})
