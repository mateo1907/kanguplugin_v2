function getAllGroups()
{
  jQuery(document).ready(function($){
    var data = {
      'action' : 'adminActionShowGroups',
    }

    $.ajax({
      type:'get',
      url:ajaxurl,
      data:data
    })
    .success(function(response){
      var json = $.parseJSON(response)
      var groupsTemplate = _.template($('#tmpl-groupsTemplate').html())
      console.log(json)
      $('#groupsTemplate').html(groupsTemplate(json));
    })
    .error(function(req, textStatus, errorThrown){
      console.log(textStatus,errorThrown)
    })
  })
}

jQuery(document).ready(function($){
  getAllGroups();
  $('#additionalOptions').on('click',function(){
    if ($(this).is(':checked')) {
      $('#option').addClass('options')
      $('#additionalOptionsDiv').show()
    } else {
      $('#additionalOptionsDiv').hide()
      $('#option').removeClass('options')
    }
  })
  $('#actionAddGroup').on('submit', function() {
    var options = []

    $('.options').each(function(){
      var id = $(this).attr('id')
      var val = $(this).val()
      options.push({
        id:id,
        val:val
      })
    })
    var data = {
      'action' : 'adminActionAddGroup',
      'nonce' : $('input[name=nonce]').val(),
      'name' : $('#name').val(),
      'maxQuantity' : $('#maxQuantity').val(),
      'options' : options

    }
    console.log(data)
    $.ajax({
      type:'post',
      url: ajaxurl,
      data: data
    })
    .success(function(response){
      var json = $.parseJSON(response);
      var content = flashMessageTemplate(json);
      //
      $('#messages').html(content)
      getAllGroups();
      // console.log(response)

    })
    return false;
  })





})
