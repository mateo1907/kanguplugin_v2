function getSavedPrices()
{
  jQuery(document).ready(($) => {
    let data = {
      action:'adminActionShowPrices',

    }
    $.ajax({
      method:'GET',
      url:ajaxurl,
      data:data
    })
    .success( (response) => {
      let json = $.parseJSON(response)
      let pricesTemplate = _.template($('#tmpl-prices').html())

      let content = pricesTemplate(json)
      $('#savedPrices').html(content)
      console.log(json)

    })
  })
}

jQuery(document).ready( ($) => {
  getSavedPrices()
  $('#addPrice').on('submit', () =>{
    $('#messages').empty()
    let group = $('#groups').val()
    let range = $('#range').val()
    let price = $('#price').val()
    let nonce = $('#nonce').val()
    let error = false;
    let data = {
      action: 'adminActionAddPrice',
      group:group,
      range:range,
      price:price,
      nonce:nonce
    }

    if (price == '') {
      $.alert({
        title:'Błąd',
        content:'Wprowadź poprawną wartość ceny'
      })
      error = true;
    }

    if (error == false) {
      $.ajax({
        type:'post',
        url:ajaxurl,
        data:data
      })
      .success((response) => {
      let json = $.parseJSON(response);
      let content = flashMessageTemplate(json)

      $('#messages').html(content);
      getSavedPrices()

      })
    }
    return false;
  })

  
})
