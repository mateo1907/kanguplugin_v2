jQuery(document).ready(function($){
  // console.log(moment.tz.guess())
  // var timezone = moment.tz.guess()

  $('#setHandler').on('click',function(){
    $.ajax({
      method:'post',
      url:ajaxurl,
      data:
      {
        action: 'adminActionSetHandler',
        handler: $('#handler').val()
      }
    })
    .success(function(response){
      var json = $.parseJSON(response)
      var content = flashMessageTemplate(json)

      $('#messages').html(content);

    })
  })

  $('#setProduct').on('click',function(){
    $.ajax({
      method:'post',
      url:ajaxurl,
      data:
      {
        action: 'adminActionSetSavedProduct',
        savedProduct: $('#savedProduct').val()
      }
    })
    .success(function(response){
      var json = $.parseJSON(response)
      var content = flashMessageTemplate(json)

      $('#messages').html(content);

    })
  })

  $('#setRedirectPage').on('click',function(){
    $.ajax({
      method:'post',
      url:ajaxurl,
      data:
      {
        action: 'adminActionSetRedirectPage',
        savedProduct: $('#redirectPage').val()
      }
    })
    .success(function(response){
      var json = $.parseJSON(response)
      var content = flashMessageTemplate(json)

      $('#messages').html(content);

    })
  })
})
