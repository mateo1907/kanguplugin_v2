function getAllReservations()
{
  jQuery(document).ready(function($){
    $.ajax({
      method:'get',
      url:ajaxurl,
      data: {
        action:'adminActionGetAllReservations'
      }
    })
    .success(function(response){
      var json = $.parseJSON(response)
      var reservationsTemplate = _.template($('#tmpl-reservations').html())
      // console.log(json)
      $('#reservationsTemplate').html(reservationsTemplate(json));
    })
  })
}

jQuery(document).ready(function($){
  getAllReservations()
  $('.reservations_slide').toggle(function(){
    $('#content_'+$(this).attr('data-id')).slideDown();
  }, function(){
    $('#content_'+$(this).attr('data-id')).slideUp();
  })

  jQuery('#date').datetimepicker({
    'timepicker':false,
    'format': 'Y-m-d'
  });
  jQuery('#dateFrom').datetimepicker({
    'timepicker':true,
    'format': 'Y-m-d H:i'
  });
  jQuery('#dateTo').datetimepicker({
    'timepicker':true,
    'format': 'Y-m-d H:i'
  });

  $('#checkHour').on('click',function(){
    emptyMessages()
    $('.showAfterHoursList').hide()
    $('.showAfterHourCheck').hide()
    $('.emptyBeforeLoadHours').empty()
    $('img.loader').show()

    var data = {
      action : 'adminActionGetHoursByDateAndGroup',
      date : $('#date').val(),
      group : $('.group:checked').val(),

    }
    $.ajax({
      method:'get',
      url:ajaxurl,
      data: data
    })
    .success(function(response) {
      var json = $.parseJSON(response);
      if (json.type == 'success') {
        // console.log(json.data)
        var data = json.data
        data.forEach(function(cell){
          $('#hours').append('<input data-quantity="'+cell.quantity+'" name="hour" type="radio" class="hour" value="'+cell.id+'"> <label>'+cell.title+'</label> ');
        })

        var quantity = $('#hours').attr('data-quantity');
        // console.log(quantity)
        // for (var i = 0; i < quantity; i++) {
        //   // $('#quantity').append('<option value="'+i+'">'+i+'</option>');
        //   // $('#socks_quantity').append('<option value="'+i+'">'+i+'</option>');
        //
        // }

        $('.showAfterHoursList').fadeIn('slow')
        $('img.loader').hide()
      } else {
        var content = flashMessageTemplate(json)
        $('img.loader').hide()
        $('#messages').html(content)
      }
    })
  })

  $(document).on('click','.hour',function(){
    var thisHour = $(this)
    $('.loader').show()
    $('.emptyOnHourClick').empty()
    $('.showAfterHourCheck').hide()
    var quantity = $(this).attr('data-quantity');
    var commonquantity = 0
    var option = 0
    if ($('.group:checked').attr('data-commonquantity') == 1) {
      commonquantity = 1
    }

    if ($('.group:checked').attr('data-option') == 1) {
      option = 1
    } else if ($('.group:checked').attr('data-option') == 2) {
      option = 2
    }
    console.log('commonquantity:',commonquantity)
    var data = {
      action: 'adminActionGetQuantitiesByDate',
      id: thisHour.val(),
      date: $('#date').val()
    }

    if ($('#date').val() !== '' && $('.group').is(':checked')) {
      $.ajax({
        method:'get',
        url:ajaxurl,
        data:data
      })
      .success(function(response){
        if (commonquantity == 0) {
          if (option == 1) {
            $('#quantity').prop('readonly',true)
          } else if (option == 2) {
            $('#quantity').val(quantity)
            $('#quantity').attr('min-quantity',quantity)
          }
          $('#quantity').attr('max-quantity', quantity)
          $('#socks_quantity').attr('max-quantity', quantity)
        } else {
          var json = $.parseJSON(response);
          if (option == 2) {
            $('#quantity').prop('readonly',true)
          } else if (option == 1) {
            $('#quantity').val(quantity)
            $('#quantity').attr('min-quantity',quantity)
          }
          $('#quantity').attr('max-quantity', json.data.quantity)
          $('#socks_quantity').attr('max-quantity',  json.data.quantity)
        }
        $('.showAfterHourCheck').fadeIn()
        $('.loader').hide()
      })
    }



    console.log(quantity)
    // for (var i = 1; i <= quantity; i++) {
      // $('#quantity').append('<option value="'+i+'">'+i+'</option>');
      // $('#socks_quantity').append('<option value="'+i+'">'+i+'</option>');
    // }


  })

  $('.group').on('click',function(){
    $('.showAfterHoursList').hide()
    $('.showAfterHourCheck').hide()
    $('.emptyBeforeLoadHours').empty()
  })

  $('.quantities').bind('change keyup mouseup',function(){
    if (parseFloat($(this).val()) < 0) {
      $.alert({
        title:'Błąd',
        content:'Wartość nie może być mniejsza niż 0'
      })
      $(this).val(1)
    }
    if (parseFloat($(this).val()) > parseFloat($(this).attr('max-quantity'))) {
      $.alert({
        title:'Błąd',
        content:'Wartość nie może być większa niż '+ $(this).attr('max-quantity')
      })
      $(this).val($(this).attr('max-quantity'))
    }
  })

  // Zatwierdzenie rezerwacji (onSubmit)

  $('#addReservation').on('submit',function(){
    emptyMessages()

    var firstname = $('#firstname').val(),
        lastname = $('#lastname').val(),
        group = $('#group').val(),
        hour = $('.hour:checked').val(),
        email = $('#email').val(),
        phone = $('#phone').val(),
        quantity = $('#quantity').val(),
        nonce = $('#nonce').val()
    if ($('#no_socks').is(':checked')) {
      var socks = false,
          socks_quantity = 0

    } else {
      var socks = true,
          socks_quantity = $('#socks_quantity').val()
    }

    if ( ($('.showAfterHoursList').is(':visible') == false && $('.showAfterHourCheck').is(':visible') == false) || firstname == false || lastname == false || email == false || phone == false ) {
      var json = {
        type:'danger',
        message: 'Uzupełnij wszystkie pola'
      }
      var errMessage = flashMessageTemplate(json)
      $('#messages').html(errMessage)
      return false;
    }

    if (socks_quantity > quantity) {
      var json = {
        type:'danger',
        message: 'Ilość skarpetek nie może być większa od ilości miejsc'
      }
      var errMessage = flashMessageTemplate(json)
      $('#messages').html(errMessage)
      return false;
    }


    var data = {
      action:'adminActionSaveReservation',
      nonce:nonce,
      firstname:firstname,
      lastname:lastname,
      group:group,
      hour:hour,
      email:email,
      phone:phone,
      quantity:parseInt(quantity),
      socks:socks,
      socks_quantity:socks_quantity
    }
    console.log(data);
    $.ajax({
      method:'post',
      url:ajaxurl,
      data:data
    })
    .success(function(response){
      var json = $.parseJSON(response)
      var content = flashMessageTemplate(json)

      $('#messages').html(content);
      if (json.type == 'success') {
        $('input[type=text]').val('')
        $('.showAfterHoursList').hide()
        $('.showAfterHourCheck').hide()
        $('.emptyBeforeLoadHours').empty()
        getAllReservations()
      }
      return false;
    })


    return false;

  })


  // Filtrowanie po dacie

  $('#filtr').on('click',function(){
    var from = $('#dateFrom').val(),
        to = $('#dateTo').val()


    $.ajax({
      method:'get',
      url:ajaxurl,
      data:{
        action:'adminActionGetReservationsByFilters',
        from: from,
        to:to
      }
    })
    .success(function(response){
      var json = $.parseJSON(response)
      var reservationsTemplate = _.template($('#tmpl-reservations').html())
      // console.log(json)
      $('#reservationsTemplate').html(reservationsTemplate(json));
    })
    return false;
  })

  // Usunięcie

  $(document).on('click','.deleteReservationIco',function(){
    var id = $(this).attr('data-id')
    var nonce = $('#deleteNonce').val();
    $.confirm({
      title: 'Potwierdź usunięcie',
      content: 'Czy na pewno chcesz usunąć ten wpis?',
      buttons: {
          Tak: function () {
            var data = {
              'action' : 'adminActionDeleteReservation',
              'id' : id,
              'nonce' : nonce
            }
              $.ajax({
                type:'post',
                url:ajaxurl,
                data:data
              })
              .success(function(response){
                getAllReservations();
                var json = $.parseJSON(response);
                var content = flashMessageTemplate(json);
                //
                $('#messages').html(content)

              })
          },
          Nie: function () {

          },

      }
  });
  })
})
