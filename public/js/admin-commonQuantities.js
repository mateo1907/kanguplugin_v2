jQuery(document).ready(function($){

  $('#setCommonQuantities').on('click',function(){
    var quantitiesArray = [];
    $('.loader').show()
    $('#messages').empty()
    $('.commonQuantity').each(function(){
      var id = $(this).attr('data-id')
      var quantity = $(this).val()
      if (quantity == '') {
        $.alert({
          title:'Błąd',
          content: 'Uzupełnij wszystkie pola'
        })
        return false;
      }
      quantitiesArray.push({
        id : id,
        quantity: quantity
      })
    })

    var data = {
      action : 'adminActionSetCommonQuantities',
      nonce: $('#nonce').val(),
      data:quantitiesArray
    }
    $.ajax({
      method:'post',
      url:ajaxurl,
      data:data
    })
    .success(function(response){
      var json = $.parseJSON(response)
      var content = flashMessageTemplate(json)
      $('.loader').hide()
      $('#messages').html(content)
    })
  })
})
