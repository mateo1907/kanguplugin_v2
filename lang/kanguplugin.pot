#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-01-09 14:10+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco - https://localise.biz/"

#: includes/class/KanguPlugin/Ajax.php:293
msgid "Dla wybranej daty i grupy nie ma przydzielonych godzin"
msgstr ""

#: includes/class/KanguPlugin/Ajax.php:296
msgid "Wprowadź poprawną datę"
msgstr ""

#: includes/class/KanguPlugin/Ajax.php:443
msgid "Brak ceny dla wybranych kryteriów"
msgstr ""

#: includes/class/KanguPlugin/Front.php:13
msgid "Wartość nie może być większa niż "
msgstr ""

#: includes/views/front/frontReservationBlock.php:5
msgid "Rezerwacja"
msgstr ""

#: includes/views/front/frontReservationBlock.php:10
msgid "Wybierz grupę:"
msgstr ""

#: includes/views/front/frontReservationBlock.php:19
msgid "Wybierz datę"
msgstr ""

#: includes/views/front/frontReservationBlock.php:21
msgid "Kontynuuj"
msgstr ""

#: includes/views/front/frontReservationBlock.php:32
msgid "Ilość miejsc"
msgstr ""

#: includes/views/front/frontReservationBlock.php:34
msgid "Ilość skarpetek"
msgstr ""

#: includes/views/front/frontReservationBlock.php:36
msgid "Nie zamawiam skarpetek"
msgstr ""

#: includes/views/front/frontReservationBlock.php:45
msgid "Rezerwuj termin"
msgstr ""

#. Name of the plugin
msgid "KanguJump Plugin v2"
msgstr ""

#. Description of the plugin
msgid "Poprawiona wersja pluginu rezerwacji KanguJump"
msgstr ""

#. Author of the plugin
msgid "Mateusz Prasał"
msgstr ""
